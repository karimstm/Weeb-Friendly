import React, { useEffect, useRef, useState } from "react";
import Modal from "react-bootstrap/Modal";
import { reloadPage } from "../../App";
import Consts from "../../classes/Consts";
import User from "../../classes/User";
import { waitForPromised } from "../../jsHelpers/jifa";
import MALUtils from "../../utils/MAL";
import { displayToast } from "./ToastMessages";

export const Login: React.FC<{}> = () => {
	const [isLoggedIn, setIsloggedIn] = useState(Consts.MAL_USER.isLoggedIn),
		frameRef = useRef<HTMLIFrameElement>(null);

	const handleClose = () => {
		Consts.setWantsToLogin(false);
		setIsloggedIn(false);
		reloadPage();
	};

	useEffect(() => {
		if (frameRef.current && !frameRef.current.src)
			MALUtils.getLoginRedirectUrl().then(([authURL, codeChallenge]) => {
				const frame = frameRef.current!;
				frame.src = authURL;
				waitForPromised(() => frame.contentWindow?.location.pathname === "/").then(() => handleIframeDone(frame, codeChallenge));
			});
	});

	const handleIframeDone = (frame: HTMLIFrameElement, codeChallenge: string) => {
		const { code, error, message, hint } = Object.fromEntries([...new URLSearchParams(frame.contentWindow!.location.search).entries()]);
		if (error === "access_denied" && hint === "The user denied the request") {
			Consts.setWantsToLogin(false);
			displayToast({
				title: "Got Error from MAL Login!",
				body: "You need to press \"Allow\" in order to login to MAL! Otherwise I have no access to your account :("
			});
			reloadPage();
		}
		else if (error) {
			Consts.setWantsToLogin(false);
			displayToast({
				title: "Got Error from MAL Login!",
				body: `Got an unknown error! Please contact guydht! Error: ${error}\n${message}`
			});
			reloadPage();
		}
		MALUtils.setMalChallengeResult(code, codeChallenge);
		onLogin();
	}, onLogin = async () => {
		displayToast({
			title: "Successfully logged in!",
			body: "I'll be fetching your MAL data right now in the background!"
		});
		Consts.setMALUser(new User('', '', undefined, true));
		setIsloggedIn(true);
		await MALUtils.getUserAnimeList(Consts.MAL_USER);
		displayToast({
			title: "Fetched List Successfully!",
			body: "Successfully fetched your myanimelist data!"
		});
		Consts.setMALUser(Consts.MAL_USER);
		reloadPage();
	};
	return (
		<Modal
			show={!isLoggedIn && Consts.WANTS_TO_LOGIN_TO_MAL}
			onHide={handleClose}
			centered>
			<iframe title="Myanimelist Login Page" height="800" ref={frameRef}></iframe>
		</Modal>
	);
};

// export default class Login extends Component<{ username?: string, password?: string; }> {
// 	static Alert_SHOW_TIMEOUT = 2000;

// 	state = {
// 		isLoggedIn: Consts.MAL_USER.isLoggedIn
// 	};
// 	frameRef = React.createRef<HTMLIFrameElement>();

// 	render() {
// 		const handleClose = () => {
// 			Consts.setWantsToLogin(false);
// 			this.setState({ isLoggedIn: false });
// 		};

// 		return (
// 			<Modal
// 				show={!this.state.isLoggedIn && Consts.WANTS_TO_LOGIN_TO_MAL}
// 				onHide={handleClose}
// 				centered>
// 				<iframe title="Myanimelist Login Page" height="500" ref={this.frameRef} src="https://myanimelist.net/login.php"></iframe>
// 			</Modal>
// 		);
// 	}
// }
