import classNames from "classnames";
import { Range } from 'rc-slider';
import 'rc-slider/assets/index.css';
import React, { Component, useMemo, useRef, useState } from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Col from "react-bootstrap/Col";
import ListGroup from "react-bootstrap/ListGroup";
import Modal from "react-bootstrap/Modal";
import Overlay from "react-bootstrap/Overlay";
import Popover from "react-bootstrap/Popover";
import Row from "react-bootstrap/Row";
import Spinner from "react-bootstrap/Spinner";
import Table from "react-bootstrap/Table";
import { reloadPage } from '../../App';
import { ReactComponent as DownloadIcon } from "../../assets/download.svg";
import AnimeEntry from "../../classes/AnimeEntry";
import Consts from "../../classes/Consts";
import DownloadedItem from "../../classes/DownloadedItem";
import TorrentManager from "../../classes/TorrentManager";
import ChooseSource from '../../components/ChooseSource';
import DownloadedFileThumbnail from "../../components/DownloadedFileThumbnail";
import SearchBar from "../../components/SearchBar";
import animeStyles from "../../css/pages/DownloadedAnime.module.css";
import styles from "../../css/pages/Episodes.module.css";
import { groupByEpisode } from "../../utils/general";
import { formatTimeAgo } from "../../utils/OutsourcedGeneral";
import TorrentUtils, { SearchResult, SearchResultExtraInfo, Sources } from "../../utils/torrents";
import { AnimeInfoProps } from "../AnimeInfo";
import { displayToast } from '../global/ToastMessages';


type SortConfig<T = any> = { key: keyof T, direction: "ascending" | "descending"; customSort?: (a: T, b: T) => -1 | 0 | 1; };
const useSortableData: <T>(items: T[], initialConfig?: SortConfig<T>) => { items: T[], requestSort: (key: keyof T) => void, sortConfig?: SortConfig<T>; } = (items, initialConfig) => {
	const [sortConfig, setSortConfig] = useState(initialConfig);

	const doSort = () => {
		let sortableItems = [...items];
		if (sortConfig !== undefined) {
			if (sortConfig.customSort)
				sortableItems.sort(sortConfig.customSort);
			else
				sortableItems.sort((a, b) => {
					const aValue = a[sortConfig.key],
						bValue = b[sortConfig.key];
					if (typeof aValue === "string" && typeof bValue === "string")
						return (sortConfig.direction === 'ascending' ? 1 : -1) * aValue.localeCompare(bValue, undefined, { ignorePunctuation: true, numeric: true });
					if (a[sortConfig.key] < b[sortConfig.key]) {
						return sortConfig.direction === 'ascending' ? -1 : 1;
					}
					if (a[sortConfig.key] > b[sortConfig.key]) {
						return sortConfig.direction === 'ascending' ? 1 : -1;
					}
					return 0;
				});
		}
		return sortableItems;
	};

	const sortedItems = useMemo(doSort, [items, sortConfig]);

	return {
		items: sortedItems,
		requestSort: key => {
			let direction: SortConfig["direction"] = 'descending';
			if (sortConfig?.key === key && sortConfig?.direction === 'descending')
				direction = 'ascending';
			setSortConfig({ key, direction });
		},
		sortConfig
	};
};

const SortableTable: React.FC<{ searchResults: SearchResult[]; startDownload: any; }> = ({ searchResults, startDownload }) => {
	const { items, requestSort, sortConfig } = useSortableData(searchResults, {
		direction: "descending",
		key: "name"
	});
	return (
		<Table striped bordered hover responsive size="xl" className={styles.resultsTable}>
			<thead>
				<tr>
					<th title="Click For More Info!" className={classNames(styles.name, {
						[styles.ascending]: sortConfig?.key === "name" && sortConfig.direction === "ascending",
						[styles.descending]: sortConfig?.key === "name" && sortConfig.direction === "descending"
					})} onClick={() => requestSort("name")}>Name</th>
					<th title="Total Size Torrent" className={classNames(styles.size, {
						[styles.ascending]: sortConfig?.key === "fileSize" && sortConfig.direction === "ascending",
						[styles.descending]: sortConfig?.key === "fileSize" && sortConfig.direction === "descending"
					})} onClick={() => requestSort("fileSize")}>Size</th>
					<th title="Date Uploaded" className={classNames(styles.date, {
						[styles.ascending]: sortConfig?.key === "timestamp" && sortConfig.direction === "ascending",
						[styles.descending]: sortConfig?.key === "timestamp" && sortConfig.direction === "descending"
					})} onClick={() => requestSort("timestamp")}>Date</th>
					<th title="Seeders" className={classNames(styles.seed, {
						[styles.ascending]: sortConfig?.key === "seeders" && sortConfig.direction === "ascending",
						[styles.descending]: sortConfig?.key === "seeders" && sortConfig.direction === "descending"
					})} onClick={() => requestSort("seeders")}>S</th>
					<th title="Leechers" className={classNames(styles.leech, {
						[styles.ascending]: sortConfig?.key === "leechers" && sortConfig.direction === "ascending",
						[styles.descending]: sortConfig?.key === "leechers" && sortConfig.direction === "descending"
					})} onClick={() => requestSort("leechers")}>L</th>
				</tr>
			</thead>
			<tbody>
				{
					items.map(searchResult => (
						<tr key={searchResult.link.magnet}>
							<CustomOverlayElement episode={searchResult} startDownload={startDownload}>
								<td className={styles.name} style={{ cursor: "pointer" }}>
									{searchResult.name}
								</td>
							</CustomOverlayElement>
							<td className={styles.size}>
								{searchResult.fileSize}
							</td>
							<td className={styles.date} title={formatTimeAgo(searchResult.timestamp)}>
								{searchResult.timestamp.toLocaleString()}
							</td>
							<td className={styles.seed}>
								{searchResult.seeders}
							</td>
							<td className={styles.leech}>
								{searchResult.leechers}
							</td>
						</tr>
					))
				}
			</tbody>
		</Table>
	);
};

const CustomOverlayElement: React.FC<{ episode: SearchResult, startDownload: any; }> = ({ episode, startDownload, children, ...props }) => {
	const [didClick, setDidClick] = useState(false),
		mainButtonRef = useRef();
	return (
		<>
			{
				React.cloneElement(React.Children.only(children) as any, {
					...props, ref: mainButtonRef,
					onClick: () => setDidClick(!didClick)
				})
			}
			<Overlay target={mainButtonRef.current as any}
				show={didClick}
				rootClose={true}
				rootCloseEvent="mousedown"
				placement="bottom"
				onHide={() => setDidClick(false)}>
				<Popover id={"popover-basic-" + episode.episodeData.quality}>
					<Popover.Title as="h3">
						Download {episode.name}:
                            </Popover.Title>
					<CustomPopover episode={episode} startDownload={startDownload} />
				</Popover>
			</Overlay>
		</>
	);
};

class CustomPopover extends Component<{ episode: SearchResult, startDownload: any; }> {
	state: { extraInfo?: SearchResultExtraInfo; } = {};
	async componentDidMount() {
		this.setState({
			extraInfo: await TorrentUtils.torrentExtraInfo(this.props.episode.link.page as string)
		});
	}
	render() {
		const { episode, startDownload } = this.props;
		return (
			<Popover.Content>
				<Row className="mb-2">
					<Col style={{ minHeight: 0 }}>
						<DownloadIcon style={{ cursor: "pointer" }} onClick={() => startDownload(episode.link.magnet, episode)} />
					</Col>
					<Col style={{ minHeight: 0 }}>
						<svg viewBox="0 0 56 18" className={styles.svgText}>
							<text fill="green">Seeders:</text>
						</svg>
						<span className={styles.seedersText}>{episode.seeders}</span>
					</Col>
					<Col style={{ minHeight: 0 }}>
						<svg viewBox="0 0 56 18" className={styles.svgText}>
							<text fill="red">Leechers:</text>
						</svg>
						<span className={styles.seedersText}>{episode.leechers}</span>
					</Col>
					{
						!!this.state.extraInfo?.size &&
						<Col style={{ minHeight: 0 }}>
							Total Files Size: <b>{this.state.extraInfo.size}</b>
						</Col>
					}
				</Row>
				{
					!!this.state.extraInfo?.description &&
					<Card className={styles.descriptionContainer}>
						<Card.Title>
							Description
                    </Card.Title>
						<Card.Body>
							{
								this.state.extraInfo?.description.split("\n").map(ele => <p key={ele}>{ele}</p>)
							}
						</Card.Body>
					</Card>
				}
				{
					!!this.state.extraInfo?.fileList.length &&
					<Card className={styles.fileListContainer}>
						<Card.Title>
							Files
                    </Card.Title>
						<Card.Body>
							<ListGroup>

								{
									this.state.extraInfo?.fileList.map(file => (
										<ListGroup.Item className={styles.fileListItem} key={file}>{file}</ListGroup.Item>
									))
								}
							</ListGroup>
						</Card.Body>
					</Card>
				}
				{
					(!!this.state.extraInfo?.comments.length &&
						<Card className={styles.commentsContainer}>
							<Card.Title>
								Comments
                    		</Card.Title>
							<Card.Body>
								{
									this.state.extraInfo?.comments.map(comment => {
										return (
											<Row key={comment.text} className={styles.commentBox}>
												<Col md="5">
													<Row>
														{comment.author}
													</Row>
													<Row>
														<img className={styles.commenterAvatar} src={comment.authorImage} alt={comment.author} />
													</Row>
												</Col>
												<Col md="7">
													<Row>
														{comment.date.toLocaleString()}
													</Row>
													<Row>
														{comment.text}
													</Row>
												</Col>
											</Row>
										);
									})
								}
							</Card.Body>
						</Card>)
					|| <p>No Comments :(</p>
				}
			</Popover.Content>
		);
	}
}

export class DisplayEpisodes extends Component<AnimeInfoProps & { source: Sources; }>{

	downloadedFromSeries: DownloadedItem[] = [];

	state: {
		episodes: SearchResult[],
		loading: boolean,
		chosenForDownload: number[],
		displayDownloadedOnly: boolean;
	} = {
			episodes: [],
			loading: true,
			chosenForDownload: [],
			displayDownloadedOnly: false
		};

	componentDidMount() {
		this.loadEpisodes();
	}

	loadEpisodes() {
		this.searchAnime(this.props.anime, this.props.source).then(episodes => {
			this.setState({
				episodes,
				loading: false
			});
		});
	}

	searchDownloadedFromSeries() {
		this.downloadedFromSeries = Consts.DOWNLOADED_ITEMS.filter(item => {
			return !isNaN(item.animeEntry.malId!) && (item.animeEntry.malId === this.props.anime.malId ||
				(item.animeEntry.name?.match(/[a-zA-Z0-9\s]*/g) || []).join("") === (this.props.anime.name?.match(/[a-zA-Z0-9\s]*/g) || []).join(""));
		});
	}

	render() {
		this.searchDownloadedFromSeries();
		const grouped = groupByEpisode(this.state.episodes),
			chosenForDownload = this.chosenForDownload;
		if (this.state.loading)
			return (
				<div className="mx-auto" style={{ width: "fit-content", textAlign: "center" }}>
					<div>Loading Episodes of {this.props.anime.name} from {Sources[this.props.source]}</div>
					<Spinner animation="grow" />
				</div>
			);
		if (!this.state.episodes.length)
			return this.couldntFindEpisodesComponent;
		return (
			<>
				{
					grouped.length > 1 && (
						<div className="mb-3">
							{
								//@ts-ignore
								<Range max={grouped.length} min={1}
									value={chosenForDownload}
									onChange={(chosenForDownload: any) => this.setState({ chosenForDownload })} />
							}

							<Button onClick={() => this.confirmDownloadEpisodes(chosenForDownload)}>
								Download Episode {chosenForDownload[0]} - {chosenForDownload[1]} ({chosenForDownload[1] - chosenForDownload[0] + 1} Episodes)?
                        </Button>
                    (In {Consts.QUALITY_PREFERENCE[0]}p, according to your quality preferences in the settings.)
						</div>
					)
				}
				<div className={styles.grid} style={{ maxHeight: "initial" }}>
					{
						grouped.map(episode => {
							const downloadedItemOfEpisode = this.downloadedItemOfEpisode(episode);
							return (
								<Card key={episode.name} className="m-1">
									<Card.Header>
										<Card.Title className={episode.animeEntry.isUserInterested() ? (
											episode.seenThisEpisode() ? animeStyles.seenEpisode : animeStyles.notSeenEpisode
										) : ""}>
											{
												isNaN(episode.episodeData.episodeOrMovieNumber) ? episode.episodeData.anime_title || "Unknown Episode" :
													`Episode ${episode.episodeData.episodeOrMovieNumber}`
											}
										</Card.Title>
										<Card.Subtitle>
											{
												episode.category?.label
											}
										</Card.Subtitle>
										{
											!!downloadedItemOfEpisode && <DownloadedFileThumbnail style={{ height: "auto" }} downloadedItem={downloadedItemOfEpisode} />
										}
									</Card.Header>
									<Card.Body>
										<SortableTable searchResults={episode.searchResults} startDownload={this.startDownload} />
									</Card.Body>
								</Card>
							);
						})
					}
				</div>
			</>
		);
	}

	get chosenForDownload() {
		if (this.state.chosenForDownload.length)
			return this.state.chosenForDownload;
		return [1, groupByEpisode(this.state.episodes).length];
	}

	downloadedItemOfEpisode(episode: SearchResult): DownloadedItem | undefined {
		return this.downloadedFromSeries.find(downloadedItem => {
			return downloadedItem.episodeData.episodeOrMovieNumber === episode.episodeData.episodeOrMovieNumber;
		});
	}
	startDownload = (magnetLink: string, episode: SearchResult) => {
		let anime = this.props.anime.syncGet();
		anime.synonyms.add(episode.episodeData.anime_title);
		anime.syncPut();
		reloadPage();
		TorrentManager.add({ magnetURI: magnetLink });
		displayToast({
			title: "Download Successfully started",
			body: `Started downloading ${episode.episodeData.name}.`
		});
	};
	confirmDownloadEpisodes([episodeStart, episodeEnd]: number[]) {
		const episodes = groupByEpisode(this.state.episodes);
		episodes.slice(episodes.length - episodeEnd, episodes.length - episodeStart + 1).forEach(episode => {
			const chosenEpisode = episode.searchResults.sort((a, b) => {
				const aVal = Consts.QUALITY_PREFERENCE.indexOf([...Consts.QUALITY_PREFERENCE].sort((q1, q2) => Math.abs(q1 - a.episodeData.quality) - Math.abs(q2 - a.episodeData.quality))[0]),
					bVal = Consts.QUALITY_PREFERENCE.indexOf([...Consts.QUALITY_PREFERENCE].sort((q1, q2) => Math.abs(q1 - b.episodeData.quality) - Math.abs(q2 - b.episodeData.quality))[0]);
				return aVal - bVal;
			})[0];
			this.startDownload(chosenEpisode.link.magnet, episode);
		});
	}
	async searchAnime(anime: AnimeEntry, source: Sources = Sources.Any, fetchAll = false): Promise<SearchResult[]> {
		const episodes = await TorrentUtils.search(anime, fetchAll, source);
		episodes.forEach(episode => episode.episodeData.anime_title = this.props.anime.name ?? "");
		episodes.sort((a, b) => {
			return b.episodeData.episodeOrMovieNumber - a.episodeData.episodeOrMovieNumber;
		});
		return episodes;
	}
	userChoseAnime = (anime: AnimeEntry) => {
		this.props.anime.syncGet();
		this.props.anime.synonyms.add(anime.name!);
		this.props.anime.syncPut(true);
		this.loadEpisodes();
		this.setState({
			loading: false
		});
	};

	couldntFindEpisodesComponent = (
		<div className="mx-1 mt-3">
			<Modal.Dialog className={styles.modal}>
				<Modal.Header>
					<Modal.Title>Couldn't Find any episodes for {this.props.anime.name} :(</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					Try and search it manually:
            <SearchBar gotoAnimePageOnChoose={false} showImage={false} onItemClick={this.userChoseAnime.bind(this)}
						onInputChange={e => this.searchAnime(new AnimeEntry({ name: (e.target as any).value }), this.props.source)
							.then(results => e.setResults(results.map(ele => ele.animeEntry).filter((ele, i, arr) => arr.map(ele => ele.name).indexOf(ele.name) === i)))}
						onInputClick={e => (e.target as any).value === "" && ((e.target as any).value = this.props.anime.name)} />
				</Modal.Body>
			</Modal.Dialog>
		</div>
	);

}
export default class Episodes extends Component<AnimeInfoProps>{
	render() {
		return (
			<div className="mt-4">
				<ChooseSource render={source => (
					<div>
						{/* <span className="float-right f-right ">Display downloaded episodes only: <Form.Check type="switch" /></span> */}
						<DisplayEpisodes source={source} {...this.props} />
					</div>
				)} lazyLoad >
				</ChooseSource>
			</div>
		);
	}
}
