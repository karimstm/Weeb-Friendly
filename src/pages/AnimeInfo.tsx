import React, { Component } from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";
//@ts-ignore
import { LazyLoadComponent } from "react-lazy-load-image-component";
import ImageZoom from "react-medium-image-zoom";
import 'react-medium-image-zoom/dist/styles.css';
import AnimeEntry from "../classes/AnimeEntry";
import MALUtils from "../utils/MAL";
import Recommendations from "./animeInfo/ Recommendations";
import Details from "./animeInfo/Details";
import Episodes from "./animeInfo/Episodes";
import Forum from "./animeInfo/Forum";
import News from "./animeInfo/News";
import Pictures from "./animeInfo/Pictures";
import Reviews from "./animeInfo/Reviews";
import Stats from "./animeInfo/Stats";

interface AnimeById {
	readonly aired: any;
	readonly airing: boolean;
	readonly background: string;
	readonly broadcast: string;
	readonly duration: string;
	readonly ending_themes: string[];
	readonly episodes: number;
	readonly favorites: number;
	readonly genres: any[];
	readonly image_url: string;
	readonly licensors: any[];
	readonly mal_id: number;
	readonly members: number;
	readonly opening_themes: string[];
	readonly popularity: number;
	readonly premiered: string;
	readonly producers: any[];
	readonly rank: number;
	readonly rating: string;
	readonly related: any;
	readonly request_cache_expiry: number;
	readonly request_cached: boolean;
	readonly request_hash: string;
	readonly score: number;
	readonly scored_by: number;
	readonly source: string;
	readonly status: string;
	readonly studios: any[];
	readonly synopsis: string;
	readonly title_english: string;
	readonly title_japanese: string;
	readonly title_synonyms: any[];
	readonly title: string;
	readonly trailer_url: string;
	readonly type: string;
	readonly url: string;
}

type ThenArg<T> = T extends PromiseLike<infer U> ? U : T;

export interface AnimeInfoProps {
	info: ThenArg<ReturnType<typeof MALUtils["getAnimeInfo"]>>;
	anime: AnimeEntry;
}
let emptyFrom = {
	day: -1,
	month: -1,
	year: -1
},
	emptyProp = {
		from: { ...emptyFrom },
		to: { ...emptyFrom }
	},
	emptyAired = {
		from: new Date("undefined"),
		prop: { ...emptyProp },
		string: "",
		to: new Date("undefined")
	},
	emptyInfo = {
		aired: { ...emptyAired },
		airing: null,
		background: "",
		broadcast: "",
		duration: "",
		ending_themes: [],
		episodes: null,
		favorites: null,
		genres: [],
		image_url: "",
		licensors: [],
		mal_id: null,
		members: null,
		opening_themes: [],
		popularity: null,
		premiered: "",
		producers: [],
		rank: null,
		rating: "",
		related: {},
		request_cache_expiry: null,
		request_cached: null,
		request_hash: "",
		score: null,
		scored_by: null,
		source: "",
		status: "",
		studios: [],
		synopsis: "",
		title_english: "",
		title_japanese: "",
		title_synonyms: [],
		title: "",
		trailer_url: "",
		type: "",
		url: ""
	};

export default class AnimeInfo extends Component<{ anime?: AnimeEntry; } & React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>> {

	private PAGE_LINKS = { Details, Episodes, Reviews, Recommendations, Stats, News, Forum, Pictures };

	state: { info?: AnimeById, anime: AnimeEntry; } = {
		anime: this.props.anime ||
			(((this.props as any).location.state || {}).animeEntry || new AnimeEntry({ malId: Number((this.props as any).match.params.id) })) as AnimeEntry
	};

	componentDidMount() {
		this.loadAnimeInfo();
	}

	loadAnimeInfo() {
		MALUtils.getAnimeInfo(this.state.anime as AnimeEntry & { malId: number; }).then(info => {
			this.setState({
				info,
				anime: this.state.anime.syncGet()
			});
		});
	}

	componentDidUpdate() {
		if (!this.props.anime && this.state.anime && (this.props as any).match.params.id && this.state.anime.malId !== Number((this.props as any).match.params.id)) {
			this.setState({
				anime: (this.props as any).location.state.animeEntry,
				info: undefined
			}, () => this.loadAnimeInfo());
		}
	}

	render() {
		let props = { ...this.props };
		delete props.anime;
		delete (props as any).staticContext;
		this.state.anime.syncGet();
		return (
			<div className={`mx-5 px-5${props.className ? " " + props.className : ""}`} {...props}>
				<Row>
					<h2>
						{
							this.state.anime.name
						}
					</h2>
				</Row>
				<Row>
					<Col md="auto">
						<ImageZoom
							overlayBgColorStart="rgba(0, 0, 0, 0)"
							overlayBgColorEnd="rgba(0, 0, 0, 0.8)">
							<img style={{ cursor: "pointer" }} src={this.state.anime.imageURL} alt={this.state.anime.name} />
						</ImageZoom>
					</Col>
					<Col md="auto" style={{ flex: 1 }}>
						<Tabs id="mal-links" defaultActiveKey={"Details"} className="justify-content-start">
							{
								Object.entries(this.PAGE_LINKS).map(([name, MyComponent], i) => {
									return (
										<Tab eventKey={name} title={name} mountOnEnter={true} key={i}>
											<LazyLoadComponent>
												<div>
													<MyComponent key={this.state.anime.malId} anime={this.state.anime} info={this.state.info || { ...emptyInfo } as any} />
												</div>
											</LazyLoadComponent>
										</Tab>
									);
								})
							}
						</Tabs>
					</Col>
				</Row>
			</div>
		);
	}
}
