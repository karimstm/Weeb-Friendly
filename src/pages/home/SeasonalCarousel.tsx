import React, { Component } from "react";
import Carousel from "react-bootstrap/Carousel";
import Spinner from "react-bootstrap/Spinner";
//@ts-ignore
import { LazyLoadImage } from "react-lazy-load-image-component";
import { Link } from "react-router-dom";
import downloadedAnimeStyle from "../../css/pages/DownloadedAnime.module.css";
import styles from "../../css/pages/SeasonalCarousel.module.css";
import MALUtils from "../../utils/MAL";
import { chunkArray, formatTimeAgo } from "../../utils/OutsourcedGeneral";


type ThenArg<T> = T extends PromiseLike<infer U> ? U : T;

export default class SeasonalCarousel extends Component {
	static readonly GRID_SIZE_X = 4;
	static readonly GRID_SIZE_Y = 2;

	state: { seasonalAnimes: ThenArg<ReturnType<typeof MALUtils["seasonalAnime"]>>; } = {
		seasonalAnimes: []
	};

	componentDidMount() {
		MALUtils.seasonalAnime().then(seasonalAnimes => {
			this.setState({ seasonalAnimes });
		});
	}

	render() {
		if (!this.state.seasonalAnimes.length)
			return (
				<div>
					<small className="d-block">Loading Seasonal...</small>
					<Spinner animation="grow" />
				</div>
			);
		return (
			<div className="mt-5">
				<h1>
					Current Seasonal:
            </h1>
				<Carousel interval={null} className="px-5 mx-5 mt-5">
					{
						chunkArray(this.state.seasonalAnimes, SeasonalCarousel.GRID_SIZE_X * SeasonalCarousel.GRID_SIZE_Y)
							.map(arrayChunk => {
								return (
									<Carousel.Item key={arrayChunk[0].name} className={styles.carousel}>
										<div className={downloadedAnimeStyle.grid}>
											{
												arrayChunk.map(seasonalAnime => {
													return (
														<div key={seasonalAnime.malId}>
															<Link to={{
																pathname: "/anime/" + seasonalAnime.malId,
																state: {
																	animeEntry: seasonalAnime
																}
															}}
																className={styles.link}>
																<span className={styles.upperTitle}>
																	{!isNaN(seasonalAnime.broadcast.getTime()) ? (
																		`Next Episode ${formatTimeAgo(seasonalAnime.broadcast)}`
																	) : 'Unknown When Next Episode'}
																</span>
																<LazyLoadImage src={seasonalAnime.imageURL}
																	className={styles.image} />
																<div className={styles.cover}></div>
																<span className={styles.title}>{seasonalAnime.name}</span>
															</Link>
														</div>
													);
												})
											}
										</div>
									</Carousel.Item>
								);
							})
					}
				</Carousel>
			</div>
		);
	}
}
