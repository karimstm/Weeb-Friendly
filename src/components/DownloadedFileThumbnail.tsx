import React, { Component } from "react";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
//@ts-ignore
import { LazyLoadComponent } from "react-lazy-load-image-component";
import { RouteComponentProps, withRouter } from "react-router";
import Consts from "../classes/Consts";
import DownloadedItem from "../classes/DownloadedItem";
import styles from "../css/pages/DownloadedAnime.module.css";
import { displayModal } from "../pages/global/ModalViewer";
import { DisplayTorrentEntry } from "../pages/home/LatestTorrents";
import { Confirm } from "../utils/OutsourcedGeneral";
import HasSeen from "./HasSeen";
import SearchBar from "./SearchBar";
import VideoThumbnail from "./VideoThumbnail";

interface DownloadedFileThumbnailProps {
	downloadedItem: DownloadedItem;
	className?: string;
	disableDoubleClick?: boolean;
	noDeleteButton?: boolean;
}

export default withRouter(class DownloadedFileThumbnail extends Component<DownloadedFileThumbnailProps & RouteComponentProps & React.HTMLProps<HTMLDivElement>> {
	render() {
		const downloadedItem: DownloadedItem = this.props.downloadedItem,
			props: any = { ...this.props };
		delete props.downloadedItem;
		delete props.history;
		delete props.location;
		delete props.match;
		delete props.disableDoubleClick;
		delete props.downloadedItem;
		delete props.noDeleteButton;
		delete props.staticContext;
		return (
			<div {...props}
				className={(this.props.className || "") + " " + styles.gridElement}>
				<HasSeen hasSeen={downloadedItem.seenThisEpisode()} />
				<div className={styles.coverElement}
					onDoubleClick={e => this.props.disableDoubleClick !== false && this.showAnime(downloadedItem) && e.stopPropagation()}
					onClick={() => this.showVideo(downloadedItem)} />
				<LazyLoadComponent>
					<VideoThumbnail
						videoUrl={Consts.FILE_URL_PROTOCOL + downloadedItem.absolutePath}
						className={styles.thumbnail}
					/>
				</LazyLoadComponent>
				{
					this.props.noDeleteButton !== true &&
					<span
						className={"mr-2 mt-1 p-1 " + styles.deleteButton} onClick={e => this.confirmDeletion() && e.stopPropagation()}>
						<span aria-hidden="true">×</span>
					</span>
				}
				<OverlayTrigger overlay={<Tooltip id="">{downloadedItem.fileName}</Tooltip>} trigger={["focus", "hover"]} placement="top" >
					<span className={styles.title}>{downloadedItem.episodeData.name}</span>
				</OverlayTrigger>
			</div>
		);
	}
	showAnime(downloadedItem: DownloadedItem) {
		clearTimeout(this.doubleClickTimeout);
		if (downloadedItem.animeEntry.malId) {
			this.props.history.push({
				pathname: "/anime/" + downloadedItem.animeEntry.malId,
				state: {
					animeEntry: downloadedItem.animeEntry
				}
			});
		}
		else
			downloadedItem.searchInMal().then(animeEntry => animeEntry?.malId ? this.showAnime(downloadedItem) : displayModal({
				title: 'Please choose which anime is ' + this.props.downloadedItem.animeEntry.name,
				body: <SearchBar clickOnStart gotoAnimePageOnChoose showImage defaultValue={this.props.downloadedItem.animeEntry.name}
					onItemClick={entry => DisplayTorrentEntry.setMALLink(this.props.downloadedItem as any, entry)} />
			}));
	}
	waitingForDeletionConfirmation = false;
	doubleClickTimeout?: number;
	doubleClickTimeoutMiliseconds = 400;
	showVideo(videoItem: DownloadedItem) {
		clearTimeout(this.doubleClickTimeout);
		this.doubleClickTimeout = setTimeout(() => {
			if (!this.waitingForDeletionConfirmation)
				videoItem.startPlaying();
		}, this.doubleClickTimeoutMiliseconds) as unknown as number;
	}
	confirmDeletion() {
		this.waitingForDeletionConfirmation = true;
		Confirm(`Are you sure you want to delete ${this.props.downloadedItem.episodeData.name}?`, (ok: boolean) => {
			this.waitingForDeletionConfirmation = false;
			ok && this.props.downloadedItem.delete();
		});
		return true;
	}
});
