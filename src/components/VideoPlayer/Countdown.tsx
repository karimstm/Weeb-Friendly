import React, { Component } from "react";
import { CountdownCircleTimer } from "react-countdown-circle-timer";
import Consts from "../../classes/Consts";
import DownloadedItem from "../../classes/DownloadedItem";
import styles from "../../css/components/VideoPlayer.module.css";
import { waitFor } from "../../jsHelpers/jifa";
import VideoThumbnail from "../VideoThumbnail";

export class CountdownToNextEpisode extends Component<{
	nextEpisode: DownloadedItem;
}> {
	static COUNTDOWN_SECONDS = 10;

	nextEpisodeElement = React.createRef<HTMLDivElement>();

	state = {
		isWaiting: false
	};

	componentDidMount() {
		const visibilityObserver = new IntersectionObserver(intersections => {
			this.setState({ isWaiting: intersections[0].isIntersecting });
		}, {
			threshold: 0.7
		});
		waitFor(() => this.nextEpisodeElement.current, () => {
			visibilityObserver.observe(this.nextEpisodeElement.current!);
		});
	}

	render() {
		const onMouseEnter = () => this.setState({ isWaiting: false }),
			onMouseLeave = () => this.setState({ isWaiting: true }),
			startNextEpisode = () => this.props.nextEpisode.startPlaying();
		return (
			<div onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave} className={styles.centered}
				onMouseDown={startNextEpisode} ref={this.nextEpisodeElement}
				style={{
					height: "50%", width: "50%", padding: "3%", borderRadius: "10px", cursor: "pointer",
					background: "rgba(30, 30, 30, 0.9)", zIndex: 1, minHeight: "260px"
				}} >
				<div style={{ position: "relative", height: "100%", width: "100%" }}>
					<div style={{ height: "100%", width: "100%", pointerEvents: "none", background: "rgba(0, 0, 0, 0.1)", position: "absolute" }}>
						<div style={{ textAlign: "center", background: "rgba(30, 30, 30, 0.3)" }}>{this.props.nextEpisode.episodeData.name}</div>
						<div className={styles.centered}>
							<CountdownCircleTimer
								key={this.state.isWaiting.toString()}
								isPlaying={this.state.isWaiting}
								duration={CountdownToNextEpisode.COUNTDOWN_SECONDS}
								colors={[['#1e1e1e', 0]]}
								onComplete={startNextEpisode}>
								{({ remainingTime }) => <h2>{remainingTime}</h2>}
							</CountdownCircleTimer>
						</div>
					</div>
					<VideoThumbnail style={{ height: "100%", width: "100%" }}
						videoUrl={Consts.FILE_URL_PROTOCOL + this.props.nextEpisode.absolutePath} />
				</div>
			</div>
		);
	}
}
