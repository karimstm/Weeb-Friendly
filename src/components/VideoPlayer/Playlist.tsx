import React, { Component } from "react";
import { ReactComponent as PlayIcon } from "../../assets/PlayIcon.svg";
import Consts from "../../classes/Consts";
import DownloadedItem from "../../classes/DownloadedItem";
import styles from "../../css/components/VideoPlayer.module.css";
import { waitFor } from "../../jsHelpers/jifa";
import VideoThumbnail from "../VideoThumbnail";

type EpisodePosition = {
	series: {
		total: number,
		index: number;
	},
	episode: {
		total: number,
		index: number;
	};
};

export class Playlist extends Component<{
	seriesQueue: DownloadedItem[][];
	setSeriesQueue?: (seriesQueue: DownloadedItem[][]) => void;
	currentPosition: EpisodePosition;
}> {

	static DisplaySeries = class DisplaySeries extends Component<{
		series: DownloadedItem[],
		setDragData?: (episode: DownloadedItem | DownloadedItem[]) => void;
		active: boolean;
		onDrag?: (index: number) => void;
		currentPosition: EpisodePosition;
	}> {

		state = {
			renderChildren: false
		};

		containerRef = React.createRef<HTMLDivElement>();

		componentDidMount() {
			const visibilityObserver = new IntersectionObserver(intersections => {
				if (!this.state.renderChildren)
					this.setState({ renderChildren: intersections[0].isIntersecting });
			}, {
				threshold: 1
			});
			waitFor(() => this.containerRef.current, () => {
				visibilityObserver.observe(this.containerRef.current!);
			});
			waitFor(() => this.currentEpisodeRef.current, () => {
				this.currentEpisodeRef.current!.scrollIntoView({
					block: "center"
				});
				if (this.currentEpisodeRef.current!.parentNode?.parentNode)
					(this.currentEpisodeRef.current!.parentNode.parentNode as Element).scrollLeft = 0;
			});
		}

		currentEpisodeRef = React.createRef<HTMLDivElement>();

		render() {
			const onEpisodeClick = (episode: DownloadedItem) => {
				episode.startPlaying();
			},
				onSeriesDragStart = () => {
					this.props.setDragData?.(this.props.series);
				},
				onEpisodeDragStart = (e: React.DragEvent, episode: DownloadedItem) => {
					this.props.setDragData?.(episode);
					e.stopPropagation();
				},
				onEpisodeDragLeave = (e: React.DragEvent, index: number) => {
					const rect = (e.target as HTMLElement).getBoundingClientRect();
					if (e.clientY - rect.top - rect.height / 2 < 0)
						this.props.onDrag?.(index + 1);
					else
						this.props.onDrag?.(index);
				};

			return (
				<div
					ref={this.containerRef}
					draggable
					onDragStart={onSeriesDragStart}>
					<span>{this.props.series[0]?.episodeData.anime_title}</span>
					{
						this.props.series.map((episode, i) => (
							<div key={episode.fileName} onClick={() => onEpisodeClick(episode)}
								draggable
								ref={this.props.active && this.props.currentPosition.episode.index === i ? this.currentEpisodeRef : undefined}
								onDragStart={e => onEpisodeDragStart(e, episode)}
								onDragLeave={e => onEpisodeDragLeave(e, i)}>
								{this.props.active && this.props.currentPosition.episode.index === i && <PlayIcon />}
								{this.state.renderChildren ? <VideoThumbnail videoUrl={Consts.FILE_URL_PROTOCOL + episode.absolutePath} /> : <div />}
								<span>Episode {episode.episodeData.episodeOrMovieNumber}</span>
							</div>
						))
					}
				</div>
			);
		}
	};

	vidoeQueueContainer = React.createRef<HTMLDivElement>();

	componentDidMount() {
		waitFor(() => this.vidoeQueueContainer.current, () => {
			this.vidoeQueueContainer.current!.addEventListener("mousedown", e => e.stopPropagation());
			this.vidoeQueueContainer.current!.addEventListener("mouseup", e => e.stopPropagation());
		});
	}

	state: {
		currentDraggingData?: [number, DownloadedItem | DownloadedItem[]];
	} = {};

	render() {
		const onDrag = (destinationSeries: DownloadedItem[], destinationSeriesIndex: number, destinationEpisodeIndex: number) => {
			/**
			 * Dragged series - the series that is currently hovering in the drag
			 * Destination series - the series that the mouse is hovering over right now
			 */
			if (!this.state.currentDraggingData) return;
			const seriesQueue = this.props.seriesQueue,
				[draggedSeriesIndex, episodeOrSeries] = this.state.currentDraggingData,
				draggedSeries = seriesQueue[draggedSeriesIndex];
			const onSeriesDrag = () => {
				seriesQueue.splice(destinationSeriesIndex, 0, seriesQueue.splice(draggedSeriesIndex, 1)[0]);
			};
			const onSameSeriesEpisodeDrag = () => {
				destinationSeries.splice(destinationEpisodeIndex, 0, destinationSeries.splice(destinationSeries.findIndex(ele => ele.episodeData.name === (episodeOrSeries as DownloadedItem).episodeData.name), 1)[0]);
			};
			const onMergeSameSeriesEpisodeDrag = () => {
				destinationSeries.splice(destinationEpisodeIndex, 0, draggedSeries.splice(draggedSeries.findIndex(ele => ele.episodeData.name === (episodeOrSeries as DownloadedItem).episodeData.name), 1)[0]);
				if (draggedSeries.length === 0) {
					seriesQueue.splice(draggedSeriesIndex, 1);
					if (destinationSeriesIndex > draggedSeriesIndex) {
						destinationSeriesIndex--;
					}
				}
			};
			const onSplitSeriesEpisodeDrag = () => {
				if (draggedSeries.length === 1) {
					//eslint-disable-next-line
					this.state.currentDraggingData = [draggedSeriesIndex, [episodeOrSeries as DownloadedItem]];
					return onDrag(destinationSeries, destinationSeriesIndex, destinationEpisodeIndex);
				}
				seriesQueue.splice(destinationSeriesIndex, 0, draggedSeries.splice(draggedSeries.findIndex(ele => ele.episodeData.name === (episodeOrSeries as DownloadedItem).episodeData.name), 1));
			};
			if (Array.isArray(episodeOrSeries))
				onSeriesDrag();
			else if (destinationSeriesIndex === draggedSeriesIndex)
				onSameSeriesEpisodeDrag();
			else if (destinationSeries[0]?.episodeData.anime_title === draggedSeries[0]?.episodeData.anime_title)
				onMergeSameSeriesEpisodeDrag();
			else
				onSplitSeriesEpisodeDrag();
			this.setState({
				currentDraggingData: [destinationSeriesIndex, episodeOrSeries]
			});
			this.props.setSeriesQueue?.(seriesQueue);
		};
		return (
			<div className={styles.videoQueueContainer}
				ref={this.vidoeQueueContainer}>
				{
					this.props.seriesQueue.map((series, i) => (
						<Playlist.DisplaySeries key={series[0].absolutePath}
							setDragData={data => this.setState({ currentDraggingData: [i, data] })}
							onDrag={(episodeIndex) => onDrag(series, i, episodeIndex)}
							active={this.props.currentPosition.series.index === i}
							currentPosition={this.props.currentPosition}
							series={series} />
					))
				}
			</div>
		);
	}
}
