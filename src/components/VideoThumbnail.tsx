import React, { Component } from "react";

type RenderParams = {
	videoUrl: string;
	snapshotTime?: number;
	// Time in seconds in video on which to capture snapshot.
	// Values between 1 and 0 and treated as a fraction of the total video duration.
	renderedHeight?: number;
	renderedWidth?: number;
};

let waitingForVideo = 0;
const rendererVideo = document.createElement("video"),
	numberOfParallelVideoWorkers = 5,
	queue: [RenderParams, (canvas: OffscreenCanvas) => void][] = [],
	tryLoadingNextInQueue = () => {
		if (waitingForVideo < numberOfParallelVideoWorkers && queue.length) {
			waitingForVideo++;
			const params = queue.splice(0, 1)[0];
			renderVideo(params[0]).then(canvas => {
				try {
					params[1](canvas);
				} catch (e) { console.error(e); }
				finally {
					waitingForVideo--;
					tryLoadingNextInQueue();
				}
			});
		}
	},
	addToQueue = (renderParams: RenderParams, onLoaded: (canvas: OffscreenCanvas) => void, index?: number) => {
		queue.splice(index || 0, 0, [renderParams, onLoaded]);
		tryLoadingNextInQueue();
	};
export function renderVideo(renderParams: RenderParams & { returnRealCanvas?: false; }): Promise<OffscreenCanvas>;
export function renderVideo(renderParams: RenderParams & { returnRealCanvas?: true; }): Promise<HTMLCanvasElement>;
export function renderVideo(renderParams: RenderParams & { returnRealCanvas?: boolean; }) {
	let localRendererVideo = rendererVideo.cloneNode(true) as HTMLVideoElement;
	return new Promise(resolve => {
		const DEFAULT_SNAPSHOT_TIME = 0.4,
			LOADED_METADATA_STATE = 'metadata-loaded',
			LOADED_DATA_STATE = 'loaded-data',
			SUSPENDED_STATE = 'suspended-state',
			SEEKED_STATE = 'seeked',
			videoState = {
				[LOADED_METADATA_STATE]: false,
				[SUSPENDED_STATE]: false,
				[LOADED_DATA_STATE]: false,
				[SEEKED_STATE]: false
			};
		localRendererVideo.src = renderParams.videoUrl;
		localRendererVideo.onloadedmetadata = () => videoStateUpdated(LOADED_METADATA_STATE);
		localRendererVideo.onsuspend = () => videoStateUpdated(SUSPENDED_STATE);
		localRendererVideo.onloadeddata = () => videoStateUpdated(LOADED_DATA_STATE);
		localRendererVideo.onseeked = () => videoStateUpdated(SEEKED_STATE);
		function videoStateUpdated(state: string) {
			(videoState as any)[state] = true;
			if (state === LOADED_METADATA_STATE)
				videoLoadedMetadata();
			else if (videoState[SUSPENDED_STATE] && videoState[LOADED_DATA_STATE] && videoState[SEEKED_STATE])
				videoLoadedData();
		}
		function videoLoadedMetadata() {
			let snapshotTime = renderParams.snapshotTime || DEFAULT_SNAPSHOT_TIME;
			snapshotTime = Math.floor(snapshotTime > 0 && snapshotTime <= 1 ? localRendererVideo.duration * snapshotTime : snapshotTime); //Support 0.x for snapshot duration
			localRendererVideo.currentTime = snapshotTime;
		}
		function videoLoadedData() {
			let ratioOfImage = localRendererVideo.videoHeight / localRendererVideo.videoWidth,
				renderedHeight = renderParams.renderedHeight,
				renderedWidth = renderParams.renderedWidth;
			if (renderedWidth && !renderedHeight)
				renderedHeight = renderedWidth * ratioOfImage;
			else if (!renderedWidth && renderedHeight)
				renderedWidth = renderedHeight / ratioOfImage;
			let canvas;
			if (renderParams.returnRealCanvas) {
				canvas = document.createElement("canvas");
				canvas.width = renderedWidth ?? localRendererVideo.videoWidth;
				canvas.height = renderedHeight ?? localRendererVideo.videoHeight;
			}
			else
				canvas = new OffscreenCanvas(renderedWidth ?? localRendererVideo.videoWidth, renderedHeight ?? localRendererVideo.videoHeight);
			const ctx = canvas.getContext("2d");
			ctx!.drawImage(localRendererVideo, 0, 0, localRendererVideo.videoWidth, localRendererVideo.videoHeight, 0, 0, canvas.width, canvas.height);
			localRendererVideo.src = "";
			resolve(canvas);
		}
	});
}

export default class VideoThumbnail extends Component<RenderParams & React.DetailedHTMLProps<React.CanvasHTMLAttributes<HTMLCanvasElement>, HTMLCanvasElement>> {

	canvas = React.createRef<HTMLCanvasElement>();

	render() {
		let filteredProps: any = { ...this.props };
		for (let prop of ["videoUrl", "snapshotTime", "renderedHeight", "renderedWidth"])
			if (filteredProps[prop])
				delete filteredProps[prop];
		return (
			<canvas ref={this.canvas} {...filteredProps}></canvas>
		);
	}
	componentDidMount() {
		addToQueue(this.props, canvas => {
			if (this.canvas.current && canvas.height && canvas.width) {
				this.canvas.current.height = canvas.height;
				this.canvas.current.width = canvas.width;
				const bitmap = canvas.transferToImageBitmap(),
					ctx = this.canvas.current.getContext("bitmaprenderer");
				ctx?.transferFromImageBitmap(bitmap);
			}
		});
	}

}
