import { Torrent as wbTorrent } from "webtorrent";
import { reloadPage } from "../App";
import { displayToast } from "../pages/global/ToastMessages";
import Consts from "./Consts";

type Jsonify<T> = T extends Date
	? string
	: T extends object
	? {
		[k in keyof T]: Jsonify<T[k]>;
	}
	: T;

export type Torrent = Jsonify<wbTorrent> & { destroyed: boolean; id: string; };

const path = window.require("path"),
	fs = window.require("fs"),
	{ ipcRenderer } = window.require("electron");


enum ListenerTypes {
	addedTorrent,
	removedTorrent,
	torrentUpdated,
	torrentFinished
}

class Listener {
	constructor({ type, func, once }: { type: ListenerTypes, func: Function; once: boolean; }) {
		this.type = type;
		this.func = func;
		this.once = once;
	}
	type: ListenerTypes;
	func: Function;
	once: boolean;
}
class TorrentListener {
	constructor({ type, func, torrent, once }: { type: ListenerTypes, func: Function; torrent: Torrent; once: boolean; }) {
		this.type = type;
		this.func = func;
		this.torrent = torrent;
		this.once = once;
	}
	type: ListenerTypes;
	func: Function;
	torrent: Torrent;
	once: boolean;
}

export default class TorrentManager {
	static waitingForDownload: { name: string, magnetURI: string; }[] = [];
	static async add({ magnetURI, alertWhenAdded = true }: { magnetURI: string; alertWhenAdded?: boolean; }) {
		let returnedTorrent: Torrent = {
			magnetURI: magnetURI,
			name: decodeURIComponent(magnetURI.split("&").find(ele => ele.startsWith("dn="))?.substring(3) ?? 'Unknown Name').replace(/\+/g, ' '),
			id: magnetURI.slice(20, 60)
		} as Torrent;
		if (Consts.SAVED_TORRENTS)
			Consts.addToSavedTorrents(returnedTorrent);
		const torrentsLength = await ipcRenderer.invoke("sendToWebtorrentWindow", "torrentsLength");
		if (torrentsLength < Consts.MAX_NUMBER_OF_SIMULTANIOUS_TORRENTS) {
			returnedTorrent = JSON.parse(await ipcRenderer.invoke("sendToWebtorrentWindow", "addTorrent", magnetURI, { path: Consts.DOWNLOADS_FOLDER }));
			const onFinish = async () => {
				if (TorrentManager.waitingForDownload.length)
					await TorrentManager.add({ magnetURI: TorrentManager.waitingForDownload.pop()!.magnetURI });
				const files = [...returnedTorrent.files];
				await this.destroy(returnedTorrent);
				files.forEach((file: any) => {
					const absolutePath = path.join(returnedTorrent.path, file.path),
						newAbsolutePath = path.join(Consts.DOWNLOADS_FOLDER, file.path);
					if (newAbsolutePath !== absolutePath)
						fs.rename(absolutePath, newAbsolutePath);
					Consts.reloadDownloads().then(() => reloadPage());
				});
				displayToast({
					title: "Finished downloading Torrent!",
					body: `Torrent ${returnedTorrent.name} finished downloading!`
				});
				Consts.removeFromSavedTorrents(returnedTorrent);
				TorrentManager.dispatchEvent(returnedTorrent, ListenerTypes.removedTorrent);
			};
			if (returnedTorrent.progress === 1)
				onFinish();
			TorrentManager.addTorrentListener(returnedTorrent, ListenerTypes.torrentFinished, onFinish, true);
			Consts.SAVED_TORRENTS[returnedTorrent.id] = returnedTorrent;
			if (alertWhenAdded)
				TorrentManager.dispatchEvent(returnedTorrent, ListenerTypes.addedTorrent);
			reloadPage();
		}
		else
			TorrentManager.waitingForDownload.push(returnedTorrent);
		return returnedTorrent;
	}
	static getAll() {
		return Object.values(Consts.SAVED_TORRENTS);
	}
	static async serverOfTorrent(torrent: Torrent) {
		return JSON.parse(await ipcRenderer.invoke("sendToWebtorrentWindow", "createServer", torrent));
	}
	static remove(torrent: Torrent) {
		if (torrent.files) {
			Promise.all(torrent.files.map(async file => {
				let pathName = path.join(torrent.path, file.path);
				await new Promise(resolve => fs.unlink(pathName, (err: Error) => {
					resolve();
					err && console.error(err);
				}));
			})).then(() => {
				Consts.reloadDownloads().then(() => reloadPage());
			});
		}
		this.destroy(torrent);
		Consts.removeFromSavedTorrents(torrent);
		const indexInWaiting = TorrentManager.waitingForDownload.findIndex(waiting => waiting.magnetURI === torrent.magnetURI);
		if (indexInWaiting !== -1)
			TorrentManager.waitingForDownload.splice(indexInWaiting, 1);
		Consts.addRemovedTorrent(torrent);
		TorrentManager.dispatchEvent(torrent, ListenerTypes.removedTorrent);
	}
	static async destroy(torrent: Torrent) {
		return JSON.parse(await ipcRenderer.invoke("sendToWebtorrentWindow", "destroy", torrent));
	}
	static resume(torrent: Torrent) {
		Consts.removeFromSavedTorrents(torrent);
		return this.add({ magnetURI: torrent.magnetURI, alertWhenAdded: false });
	}

	private static listeners: Listener[] = [];
	public static torrentListeners: TorrentListener[] = [];
	static addEventListener(type: ListenerTypes, listenerFunc: (torrent: Torrent) => void, once = false) {
		const listener = new Listener({ type, func: listenerFunc, once });
		TorrentManager.listeners.push(listener);
		return listener;
	};
	static addTorrentListener(torrent: Torrent, type: ListenerTypes, listenerFunc: (torrent: Torrent) => void, once = false) {
		const listener = new TorrentListener({ torrent, type, func: listenerFunc, once });
		TorrentManager.torrentListeners.push(listener);
		return listener;
	}
	static removeEventListener(type: ListenerTypes, listenerFunc: (torrent: Torrent) => void) {
		const foundIndex = TorrentManager.listeners.findIndex(listener => listener.func === listenerFunc && listener.type === type);
		if (~foundIndex)
			TorrentManager.listeners.splice(foundIndex, 1);
	}
	static removeTorrentListener(torrent: Torrent, type: ListenerTypes, listenerFunc: (torrent: Torrent) => void) {
		const foundIndex = TorrentManager.torrentListeners.findIndex(listener => listener.func === listenerFunc
			&& listener.type === type
			&& listener.torrent.id === torrent.id);
		if (~foundIndex)
			TorrentManager.torrentListeners.splice(foundIndex, 1);
	}
	private static dispatchEvent(data: Torrent, type: ListenerTypes) {
		[...TorrentManager.listeners].forEach((listener, index) => {
			if (listener.type === type) {
				listener.func(data);
				if (listener.once)
					TorrentManager.listeners.splice(index, 1);
			}
		});
	}
	public static dispatchTorrentEvent(torrent: Torrent, type: ListenerTypes) {
		[...TorrentManager.torrentListeners].forEach((listener, index) => {
			if (listener.type === type && listener.torrent.id === torrent.id) {
				listener.func(torrent);
				if (listener.once)
					TorrentManager.torrentListeners.splice(index, 1);
			}
		});
	}
	static Listener = ListenerTypes;
}

ipcRenderer.on("torrentInfoUpdate", (_ = {}, torrent: string) => {
	TorrentManager.dispatchTorrentEvent(JSON.parse(torrent), ListenerTypes.torrentUpdated);
});

async function autoUpdater() {
	const torrents = JSON.parse(await ipcRenderer.invoke("sendToWebtorrentWindow", "getAllTorrents"));
	torrents.forEach((torrent: Torrent) => {
		TorrentManager.dispatchTorrentEvent(torrent, ListenerTypes.torrentUpdated);
	});
	setTimeout(() => {
		window.requestAnimationFrame(autoUpdater);
	}, 10000);
}
window.requestAnimationFrame(autoUpdater);

ipcRenderer.on("torrentFinished", (_ = {}, torrent: string) => {
	TorrentManager.dispatchTorrentEvent(JSON.parse(torrent), ListenerTypes.torrentFinished);
});
