import { reloadPage, setAppState } from "../App";
import { displayToast } from "../pages/global/ToastMessages";
import { handleRelations } from "../utils/general";
import MALUtils from "../utils/MAL";
import { CacheLocalStorage } from "../utils/OutsourcedGeneral";
import { EpisodeData, episodeDataFromFilename } from "../utils/torrents";
import AnimeEntry from "./AnimeEntry";
import Consts from "./Consts";

const trash = window.require("trash");


export default class DownloadedItem {
	videoSrc?: string;
	constructor(absolutePath: string, fileName: string, lastUpdated: Date, animeEntry?: AnimeEntry, episodeData?: Omit<EpisodeData, 'quality'>) {
		this.absolutePath = absolutePath;
		this.fileName = fileName;
		this.lastUpdated = lastUpdated;
		this.episodeData = episodeData ?? (episodeDataFromFilename(fileName) || {} as any);
		this.animeEntry = animeEntry ?? new AnimeEntry({ name: this.episodeData.anime_title });
		handleRelations(this as any);
	}
	absolutePath: string;
	fileName: string;
	lastUpdated: Date;
	animeEntry: AnimeEntry;
	episodeData: Omit<EpisodeData, 'quality'>;
	seenThisEpisode() {
		(window as any).CacheLocalStorage = CacheLocalStorage;
		if (!isNaN(this.episodeData.episodeOrMovieNumber) && this.animeEntry.seenEpisode(this.episodeData.episodeOrMovieNumber))
			return true;
		const savedVideoProgress = new CacheLocalStorage("videoLastTime").getItem(this.episodeData.name);
		return savedVideoProgress && savedVideoProgress.progress > 0.9;
	}
	startPlaying() {
		setAppState({
			videoItem: this
		});
	}
	equals(other: DownloadedItem) {
		const sameSeries = (this.animeEntry.malId && this.animeEntry.malId === other.animeEntry.malId) ||
			this.episodeData.anime_title === other.episodeData.anime_title;
		const sameEpisode = this.episodeData.episodeOrMovieNumber === other.episodeData.episodeOrMovieNumber;
		return sameSeries && sameEpisode;
	}
	async delete(reloadDownloads = true): Promise<void> {
		await trash(this.absolutePath);
		displayToast({
			title: 'Moved file to trash',
			body: `Moved ${this.fileName} to the trash.`
		});
		if (reloadDownloads)
			Consts.reloadDownloads().then(() => reloadPage());
	}
	async searchInMal(): Promise<AnimeEntry | undefined> {
		let results = await MALUtils.searchAnime(this.animeEntry);
		const similar = results.filter(result => result.compareAnimeNames(this.animeEntry));
		if (similar.length === 1) {
			this.animeEntry.malId = similar[0].malId;
			this.animeEntry.syncPut();
		}
		return this.animeEntry;
	}
}
