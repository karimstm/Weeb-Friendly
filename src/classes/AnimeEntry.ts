import { compareAnimeName, normalizeAnimeName } from "../utils/OutsourcedGeneral";
import { get, sync, ThumbnailManager } from "./AnimeStorage";
import { MALStatuses } from "./MalStatuses";
const fs = window.require("fs"),
	request = window.require("request");
export default class AnimeEntry {
	static SCORES = ["Appaling", "Horrible", "Very Bad", "Bad", "Average", "Fine", "Good", "Very Good", "Great", "Masterpiece"];
	constructor({
		synonyms = undefined,
		malId = undefined,
		score = undefined,
		genres = undefined,
		totalEpisodes = undefined,
		startDate = undefined,
		endDate = undefined,
		userStartDate = undefined,
		userEndDate = undefined,
		myWatchedEpisodes = undefined,
		myMalStatus = undefined,
		myMalRating = undefined,
		myRewatchAmount = undefined,
		imageURL = undefined,
		_imageURL = undefined,
		name = undefined,
		_name = undefined,
		sync = true
	}: {
		synonyms?: Set<string>,
		malId?: number,
		score?: number,
		genres?: Set<string>,
		synopsis?: string,
		totalEpisodes?: number,
		startDate?: Date,
		endDate?: Date,
		userStartDate?: Date,
		userEndDate?: Date,
		myWatchedEpisodes?: number,
		myMalStatus?: MALStatuses,
		myMalRating?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10,
		myRewatchAmount?: number,
		imageURL?: string,
		_imageURL?: string,
		name?: string,
		_name?: string,
		sync?: boolean;
	}) {
		this.malId = malId;
		this.name = normalizeAnimeName(name || _name);
		this.synonyms = synonyms ? new Set(Array.from(synonyms || []).sort()) : this.synonyms;
		if (sync && (malId || this.name))
			this.syncGet();
		this.score = score ?? this.score;
		this.genres = genres ? new Set(Array.from(genres || [])) : this.genres;
		this.totalEpisodes = totalEpisodes ?? this.totalEpisodes;
		this.startDate = startDate ? new Date(startDate) : this.startDate;
		this.endDate = endDate ? new Date(endDate) : this.endDate;
		this.userStartDate = userStartDate ? new Date(userStartDate) : this.userStartDate;
		this.userEndDate = userEndDate ? new Date(userEndDate) : this.userEndDate;
		this.myWatchedEpisodes = myWatchedEpisodes ?? this.myWatchedEpisodes;
		this.myMalStatus = myMalStatus ?? this.myMalStatus;
		this.myMalRating = myMalRating ?? this.myMalRating;
		this.myRewatchAmount = myRewatchAmount ?? this.myRewatchAmount;
		this.imageURL = ((imageURL || _imageURL) ?? this.imageURL)?.replace(/\/r\/[0-9]+x[0-9]+/g, '');
	}
	_synonyms = new Set<string>();
	get synonyms() {
		if (this.name)
			this._synonyms.add(this.name);
		return this._synonyms;
	}
	set synonyms(value: Set<string>) {
		this._synonyms = new Set([...value].map(normalizeAnimeName));
	}
	private * getNames() {
		if (this._name)
			yield this._name;
		yield* this._synonyms;
	}
	get names() { // Property for when you want to go over the names of the anime in a for loop
		return this.getNames();
	}
	compareAnimeName(animeName: string) {
		if (this.name && compareAnimeName(this.name, animeName)) return true;
		for (const name of this._synonyms)
			if (compareAnimeName(name, animeName))
				return true;
		return false;
	}
	compareAnimeNames(other: AnimeEntry) {
		if (other.name && this.compareAnimeName(other.name)) return true;
		for (const name of other._synonyms)
			if (this.compareAnimeName(name))
				return true;
		return false;
	}
	malId?: number;
	score?: number;
	get malUrl(): string {
		return `https://myanimelist.net/anime/${this.malId}/${this.name}`;
	};
	genres?: Set<String>;
	totalEpisodes?: number;
	startDate?: Date;
	endDate?: Date;
	userStartDate?: Date;
	userEndDate?: Date;
	myWatchedEpisodes?: number;
	myMalStatus?: MALStatuses;
	myMalRating?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;
	myRewatchAmount?: number;
	private _imageURL?: string;
	get imageURL() {
		if (ThumbnailManager.SAVED_THUMBNAILS_STATE && this.malId && !ThumbnailManager.SAVED_THUMBNAILS.has(this.malId) && this._imageURL) {
			let imageRequest = request(this._imageURL),
				writeStream = fs.createWriteStream(ThumbnailManager.SAVED_THUMBNAILS_PATH + this.malId);
			imageRequest.pipe(writeStream);
			writeStream.on("finish", () => {
				ThumbnailManager.SAVED_THUMBNAILS.add(this.malId!);
			});
		}
		return ThumbnailManager.SAVED_THUMBNAILS_STATE && this.malId && ThumbnailManager.SAVED_THUMBNAILS.has(this.malId) ?
			"file://" + ThumbnailManager.SAVED_THUMBNAILS_PATH + this.malId : this._imageURL;
	}
	set imageURL(value) {
		this._imageURL = value;
		if (ThumbnailManager.SAVED_THUMBNAILS_STATE && this.malId && !ThumbnailManager.SAVED_THUMBNAILS.has(this.malId) && this._imageURL) {
			let imageRequest = request(this._imageURL),
				writeStream = fs.createWriteStream(ThumbnailManager.SAVED_THUMBNAILS_PATH + this.malId);
			imageRequest.pipe(writeStream);
			writeStream.on("finish", () => {
				ThumbnailManager.SAVED_THUMBNAILS.add(this.malId!);
			});
		}
	}
	_name?: string;
	get name() {
		return this._name || this._synonyms.values().next().value;
	}
	set name(value: string | undefined) {
		this._name = normalizeAnimeName(value) || this._name;
	}
	syncPut(forceData: boolean = false) {
		sync(this, forceData);
		return this.syncGet();
	}
	readyForJSON() {
		let copy: any = { ...this };
		copy.synonyms = [...this.synonyms];
		return copy;
	}
	syncGet() {
		let inStorage = get(this);
		if (inStorage)
			Object.entries(inStorage).forEach(([key, value]) => {
				if (value !== this[key as keyof AnimeEntry])
					(this as any)[key] = value;
			});
		return this;
	}

	seenEpisode(episodeNumber: number) {
		return this.myWatchedEpisodes !== undefined && this.myWatchedEpisodes >= episodeNumber;
	}

	isUserInterested() {
		return this.myMalStatus !== undefined && (
			this.myMalStatus === MALStatuses.Completed ||
			this.myMalStatus === MALStatuses.Watching ||
			this.myMalStatus === MALStatuses["Plan To Watch"]
		);
	}

	clearUserData() {
		delete this.myMalRating;
		delete this.myMalStatus;
		delete this.myRewatchAmount;
		delete this.myWatchedEpisodes;
	}
}

(window as any).animeEntry = AnimeEntry;
