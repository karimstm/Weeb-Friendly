import AnimeEntry from "../classes/AnimeEntry";
import Consts from "../classes/Consts";
import DownloadedItem from "../classes/DownloadedItem";
import { groupBy } from "./OutsourcedGeneral";
import { EpisodeData, SearchResult } from "./torrents";

const { parseDate } = window.require("chrono-node");

function parseBroadcastToDate(broadcastTime: string): Date {
	const split = broadcastTime.match(/^[^ ]+(?=s)|.+/gi) ?? [];
	broadcastTime = (split[0] + split[1]?.slice(1)) ?? broadcastTime;
	let nextBroadcastDate = parseDate(broadcastTime);
	if (nextBroadcastDate < new Date())
		nextBroadcastDate = new Date(Number(nextBroadcastDate) + 604800000);
	return nextBroadcastDate;
}

type AnimeRelations = Record<number, {
	rangeStart: number,
	rangeEnd?: number,
	destination: {
		id: number;
		rangeStart: number;
		rangeEnd?: number;
	};
}[]>;


async function getAnimeRelations() {
	const fileContent = await fetch("https://raw.githubusercontent.com/erengy/anime-relations/master/anime-relations.txt").then(r => r.text()),
		lines = fileContent.split("\n").filter(ele => ele.match(/^- [0-9]/)),
		lineParser = (line: string) => {
			const id = Number(line.match(/[0-9]+/)),
				rangeStart = Number(line.match(/(?<=:)[0-9]+/)),
				rangeEnd = Number(line.match(/(?<=:[0-9]+-)[0-9]+/)) || undefined,
				destinationId = Number(line.match(/(?<=-> )[0-9]+/)) || id,
				destinationRangeStart = Number(line.match(/(?<=:)[0-9]+/g)?.[1]),
				destinationRangeEnd = Number(line.match(/(?<=:[0-9]+-)[0-9]+/g)?.[1]) || undefined,
				repeat = line.endsWith("!");
			const returnObject = {
				id,
				rangeStart,
				rangeEnd,
				destination: {
					id: destinationId,
					rangeStart: destinationRangeStart,
					rangeEnd: destinationRangeEnd
				}
			};
			if (repeat)
				return [returnObject, {
					...returnObject,
					id: destinationId
				}];
			return returnObject;
		},
		parsedLines = lines.map(lineParser).flat(Infinity) as {
			id: number,
			rangeStart: number,
			rangeEnd?: number,
			destination: {
				id: number,
				rangeStart: number,
				rangeEnd?: number;
			};
		}[];
	const asObject: AnimeRelations = {};
	parsedLines.forEach(parsedLine => {
		if (!(parsedLine.id in asObject))
			asObject[parsedLine.id] = [parsedLine];
		else
			asObject[parsedLine.id].push(parsedLine);
	});
	return asObject;
}


const handleRelations = (downloadedItem: { animeEntry: AnimeEntry, episodeData: EpisodeData; }) => {
	let didChange = false;
	if (downloadedItem.animeEntry.malId) {
		const relations = Consts.animeRelations[downloadedItem.animeEntry.malId];
		if (relations) {
			relations.forEach(relation => {
				if (relation.rangeStart <= downloadedItem.episodeData.episodeOrMovieNumber &&
					(!relation.rangeEnd || relation.rangeEnd >= downloadedItem.episodeData.episodeOrMovieNumber)) {
					didChange = true;
					downloadedItem.animeEntry = new AnimeEntry({ malId: relation.destination.id });
					downloadedItem.episodeData.episodeOrMovieNumber -= relation.rangeStart - relation.destination.rangeStart;
					if (downloadedItem.animeEntry.name) {
						downloadedItem.episodeData.anime_title = downloadedItem.animeEntry.name;
						// Reset the name property according to new anime_title
						downloadedItem.episodeData.name = `${downloadedItem.episodeData.anime_title}${downloadedItem.episodeData.year ? ` (${downloadedItem.episodeData.year})` : ""}${!isNaN(downloadedItem.episodeData.episodeOrMovieNumber) ? ` Episode ${downloadedItem.episodeData.episodeOrMovieNumber}` : ''}${downloadedItem.episodeData.other ? " (" + downloadedItem.episodeData.other + ")" : ''}`;
					}
				}
			});
		}
	}
	return didChange;
};


let { ipcRenderer } = window.require("electron"),
	walkDir = async (path: string) => {
		if (path)
			return (await ipcRenderer.invoke("walkDir", path)).map((ele: [string, string, Date]) => new DownloadedItem(...ele));
	};

function parseStupidAmericanDateString(dateString: string) {
	return parseDate(dateString);
}


function groupByEpisode(episodes: SearchResult[]): (SearchResult & { searchResults: SearchResult[]; })[] {
	return groupBy(episodes.map(ele => {
		(ele as any).asd = ele.episodeData.anime_title + ele.episodeData.episodeOrMovieNumber;
		return ele;
	}) as any[], 'asd').map(grouped => {
		grouped.sort((a, b) =>
			Consts.QUALITY_PREFERENCE.indexOf(closestQualityInQualityPreference(a.episodeData.quality)) -
			Consts.QUALITY_PREFERENCE.indexOf(closestQualityInQualityPreference(b.episodeData.quality)));
		grouped.forEach(group => {
			delete group.asd;
			group.searchResults = grouped;
		});
		return grouped[0];
	});
}

function groupBySeries(episodes: SearchResult[]) {
	return groupBy(episodes.map(ele => {
		(ele as any).asd = ele.animeEntry.malId || ele.episodeData.anime_title;
		return ele;
	}) as any[], 'asd').map(grouped => {
		grouped.forEach(ele => {
			delete ele.asd;
		});
		return grouped;
	});
}

function closestQualityInQualityPreference(quality: number) {
	return [...Consts.QUALITY_PREFERENCE].sort((qualityA, qualityB) => Math.abs(qualityA - quality) - Math.abs(qualityB - quality))[0];
}

async function convertSrtToASS(srt: string) {
	const tmp = window.require("tmp-promise"),
		srtToAss = window.require("srt-to-ass"),
		fs = window.require("fs").promises,
		path = window.require("path");

	const dir = await tmp.dir();
	const inputLocation = path.join(dir.path, "tmp_delete_me.srt"),
		outputLocation = path.join(dir.path, "tmp_delete_me.ass");
	await fs.writeFile(inputLocation, srt);
	await new Promise(resolve => srtToAss.convert(inputLocation, outputLocation, {
		style: {
			margin: { vertical: 22, left: 20, right: 20 },
			font: { family: "Arial", size: 20 }
		}, info: { play: { res: { x: 640, y: 360 } } }
	}, resolve));
	const content = await fs.readFile(outputLocation);
	await fs.unlink(inputLocation);
	await fs.unlink(outputLocation);
	dir.cleanup();
	return content.toString();
}

export { parseStupidAmericanDateString, walkDir, closestQualityInQualityPreference, parseBroadcastToDate, convertSrtToASS, groupByEpisode, groupBySeries, getAnimeRelations, handleRelations };
export type { AnimeRelations };

