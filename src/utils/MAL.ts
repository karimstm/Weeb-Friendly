import { Mal } from "node-myanimelist";
import pkceChallenge from "pkce-challenge";
import AnimeEntry from "../classes/AnimeEntry";
import AnimeList from "../classes/AnimeList";
import { updateInMalWhenHasInternet } from "../classes/AnimeStorage";
import Consts from "../classes/Consts";
import DownloadedItem from "../classes/DownloadedItem";
import { MALStatuses, reverseStatusMapping, statusMapping } from "../classes/MalStatuses";
import User from "../classes/User";
import { displayToast } from "../pages/global/ToastMessages";
import { parseBroadcastToDate, parseStupidAmericanDateString } from "./general";
import { Confirm, hasInternet, runWithDelay } from "./OutsourcedGeneral";

const MalApiAuth = Mal.auth(process.env.REACT_APP_MAL_API_ID);

interface Reviews {
	readonly request_cache_expiry: number;
	readonly request_cached: boolean;
	readonly request_hash: string;
	readonly reviews: Review[];
}
interface Review {
	readonly content: string;
	readonly date: Date;
	readonly helpful_count: number;
	readonly mal_id: number;
	readonly reviewer: any;
	readonly url: string;
}

let mal = window.require("jikan-node");
mal = new mal();

type HasMalId = {
	malId: Number;
};

const tempFetch = fetch;
window.fetch = async (url, options) => {
	const response = await tempFetch(url, options),
		responseText = await response.clone().text();
	if (responseText.trim().length === 0)
		return await runWithDelay(window.fetch, 1000, url, options);
	return response;
};

export default class MALUtils {
	static readonly MAX_ANIMES_PER_PAGE = 1000;
	static readonly MINIMUM_ANIMENAME_SIMILARITY = 0.8;

	static async searchAnime(anime: AnimeEntry, searchString: string = ""): Promise<AnimeEntry[]> {
		searchString = searchString || anime.name!;
		let data = (await fetch(`https://myanimelist.net/search/prefix.json?type=anime&keyword=${searchString}`).then(r => r.json()));
		if (!data) return [];
		let parsedData = data?.categories?.[0].items.map((result: any) =>
			new AnimeEntry({
				malId: result.id,
				imageURL: result.image_url,
				score: result.payload.score,
				startDate: new Date(result.payload?.aired.split(" to ")[0]),
				endDate: new Date(result.payload?.aired.split(" to ")[1]),
				name: result.name,
				sync: false
			}).syncPut()
		) ?? [];
		return parsedData;
	}
	static async topAnime(page = 1): Promise<AnimeEntry[]> {
		let data = (await mal.findTop("anime", page));
		if (!data) return [];
		return data.top.map((ele: any) =>
			new AnimeEntry({
				malId: ele.mal_id,
				endDate: parseStupidAmericanDateString(ele.end_date),
				startDate: parseStupidAmericanDateString(ele.start_date),
				score: ele.score,
				imageURL: ele.image_url,
				name: ele.title,
				sync: false
			}).syncPut()
		);
	}
	static seasonalAnime() {
		return this.getScheduledAnime();
	}
	static async getScheduledAnime(): Promise<(AnimeEntry & { broadcast: Date; })[]> {
		const htmlData = await fetch("https://myanimelist.net/anime/season/schedule").then(r => r.text()),
			resultKeys = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday", "other", "unknown"],
			arr: (AnimeEntry & { broadcast: Date; })[] = [];
		const data: Record<string, any[]> = {};
		[...new DOMParser().parseFromString(htmlData, "text/html").querySelectorAll(".seasonal-anime.js-seasonal-anime[data-genre]")].forEach(ele => {
			const day = ele.parentNode?.querySelector(".anime-header")?.textContent?.toLowerCase() ?? "unknown";
			if (!(day in data))
				data[day] = [];
			data[day].push({
				mal_id: Number(ele.querySelector(".genres.js-genre")?.id),
				name: ele.querySelector(".link-title")?.textContent,
				episodes: ele.querySelector(".eps span")?.textContent?.toLowerCase(),
				score: Number(ele.querySelector(".score[title=\"Score\"]")?.textContent),
				imageURL: (ele.querySelector(".image img") as any)?.dataset.src,
				startDate: ele.querySelector(".remain-time")?.textContent?.toLowerCase().trim()
			});
		});
		if (!Object.keys(data)) return [];
		resultKeys.forEach((key, keyIndex) => {
			if (data[key]) {
				arr.push(...data[key].map((result: any) => {
					const fromData: AnimeEntry & { broadcast: Date; } = new AnimeEntry({
						malId: result.mal_id,
						totalEpisodes: result.episodes,
						name: result.name,
						imageURL: result.imageURL,
						score: result.score,
						startDate: parseStupidAmericanDateString(result.startDate?.trim()),
						sync: false
					}).syncPut() as any;
					if (keyIndex < 7)
						fromData.broadcast = parseBroadcastToDate(key + " " + (fromData.startDate?.toTimeString() ?? ""));
					else
						fromData.broadcast = new Date(NaN);
					return fromData;
				}));
			}
		});
		arr.sort((a, b) => a.broadcast.getTime() - b.broadcast.getTime());
		return arr;
	}
	static async getUserAnimeList(user: User, listType = "all", page = 1): Promise<AnimeList> {
		const listTypeMapping: Record<string, MALStatuses | undefined> = {
			completed: MALStatuses.Completed,
			all: undefined,
			dropped: MALStatuses.Dropped,
			onhold: MALStatuses["On-Hold"],
			plantowatch: MALStatuses["Plan To Watch"],
			ptw: MALStatuses["Plan To Watch"],
			watching: MALStatuses.Watching
		};
		const account = await this.getMalAccount(),
			response = await account.user.animelist(undefined, undefined, undefined, {
				limit: this.MAX_ANIMES_PER_PAGE,
				offset: (page - 1) * this.MAX_ANIMES_PER_PAGE,
				status: listTypeMapping[listType] !== undefined ? statusMapping[listTypeMapping[listType]!] : undefined
			}).call();
		const data = response.data;
		if (!data || !data.length) return user.animeList;
		data.forEach(anime => {
			const entry = new AnimeEntry({
				malId: anime.node.id,
				myMalRating: anime.list_status.score,
				myWatchedEpisodes: anime.list_status.num_episodes_watched,
				name: anime.node.title,
				sync: false
			});
			if (anime.node.main_picture?.large)
				entry.imageURL = anime.node.main_picture?.large;
			if (anime.list_status.status)
				entry.myMalStatus = reverseStatusMapping[anime.list_status.status];
			user.animeList.loadAnime(entry.syncPut());
		});
		user.animeList.fetchedDate = new Date();
		if (data.length === this.MAX_ANIMES_PER_PAGE)
			await this.getUserAnimeList(user, listType, page + 1);
		return user.animeList;
	}
	static async getAnimeInfo(anime: AnimeEntry & HasMalId) {
		const htmlData = await fetch("https://myanimelist.net/anime/" + anime.malId).then(r => r.text()),
			parsed = new DOMParser().parseFromString(htmlData, 'text/html'),
			sidePanel = [...parsed.querySelectorAll(".borderClass > div > *")].reduce((a, b) =>
				Object.assign(a, {
					[b.querySelector('.dark_text')?.textContent?.slice(0, -1) ?? ""]: b.textContent?.replace(b.querySelector('.dark_text')?.textContent ?? "", '').trim()
				}), {} as Record<string, string>),
			data = {
				aired: sidePanel.Aired?.split(" to ").map(ele => new Date(ele)),
				airedString: sidePanel.Aired,
				broadcast: sidePanel.Broadcast,
				duration: sidePanel.Duration,
				episodes: Number(sidePanel.Episodes),
				favorites: Number(sidePanel.Favorites),
				genres: [...parsed.querySelectorAll("[itemprop=\"genre\"]")].map(ele => ele.textContent).filter(ele => ele) as string[],
				image_url: (parsed.querySelector('[itemprop="image"]') as HTMLElement)?.dataset.src,
				members: Number(sidePanel.Members.replace(/,/g, '')),
				popularity: Number(sidePanel.Popularity.slice(1)),
				premiered: sidePanel.Premiered,
				rank: Math.floor(parseFloat(sidePanel.Ranked?.slice(1)) / 10),
				related: [...parsed.querySelectorAll("table.anime_detail_related_anime tr")].reduce((a, b) =>
					Object.assign(a, {
						[b.children[0].textContent?.slice(0, -1) ?? ""]: ([...b.children[1].children] as HTMLAnchorElement[]).map(ele => ({
							name: ele.textContent ?? "",
							type: new URL(ele.href).pathname.split("/")[1],
							mal_id: Number(new URL(ele.href).pathname.split("/")[2])
						}))
					}), {} as Record<string, string[]>),
				source: sidePanel.Source,
				status: sidePanel.Status,
				studios: sidePanel.Studios?.split(", ").map(ele => ele.trim()),
				synopsis: parsed.querySelector("[itemprop=\"description\"]")?.textContent?.trim() ?? "",
				title: parsed.querySelector(".title-name")?.textContent?.trim(),
				title_english: sidePanel.English,
				title_japanese: sidePanel.Japanese,
				title_synonyms: sidePanel.Synonyms?.split(", ") ?? [],
				type: sidePanel.Type,
				score: Math.floor(parseFloat(sidePanel.Score) * 100) / 100,
				scored_by: Number(parsed.querySelector("[itemprop=\"ratingCount\"]")?.textContent)
			};
		anime.syncGet();
		anime.score = data.score;
		anime.name = data.title || data.title_english || data.title_synonyms[0];
		anime.genres = new Set(data.genres);
		anime.imageURL = data.image_url;
		anime.startDate = data.aired[0];
		anime.endDate = data.aired[1];
		anime.synonyms = new Set([...anime.synonyms, data.title, data.title_english, data.title_japanese, ...data.title_synonyms].filter(ele => ele) as string[]);
		anime.synonyms.delete(null as any);
		anime.totalEpisodes = data.episodes;
		anime.syncPut();
		return data;
	}
	static async updateAnime(anime: AnimeEntry & HasMalId, { episodes, status, score }: { episodes: number, status: MALStatuses, score?: number; }): Promise<boolean> {
		if (!hasInternet()) {
			return await new Promise(resolve => Confirm(`You don't have internet connection, but I can update it when you do have internet.`,
				(ok: boolean) => {
					if (ok) {
						updateInMalWhenHasInternet(
							anime, {
							episodes,
							status
						});
						anime.myMalStatus = status;
						anime.myWatchedEpisodes = episodes;
						anime.syncPut();
						displayToast({
							title: "Success",
							body: `Will update ${anime.name} in MAL when you have internet connectivity again!`
						});
					}
					resolve(ok);
				}));
		}
		const account = await this.getMalAccount();
		const ok = await account.anime.updateMyAnime(anime.malId, {
			num_watched_episodes: episodes,
			score: score || 0,
			status: statusMapping[status]
		}).call().catch(console.error);
		anime.syncGet();
		anime.myWatchedEpisodes = episodes;
		anime.myMalStatus = status;
		anime.myMalRating = score as any;
		anime.syncPut();
		Consts.MAL_USER.animeList.loadAnime(anime);
		return ok !== undefined;
	}
	static async getLoginRedirectUrl() {
		const { code_challenge } = pkceChallenge(),
			authURL = MalApiAuth.getOAuthUrl(code_challenge);
		return [authURL, code_challenge];
	}

	static async setMalChallengeResult(challengeResult: string, codeChallenge: string) {
		const account = await MalApiAuth.authorizeWithCode(challengeResult, codeChallenge);
		Consts.setMalApiToken(account.stringifyToken());
		return account;
	}
	static async getMalAccount() {
		const token = Mal.MalToken.fromJsonString(Consts.MAL_API_TOKEN);
		return MalApiAuth.loadToken(token);
	}
	static async addAnime(anime: AnimeEntry & HasMalId) {
		return await this.updateAnime(anime, {
			episodes: 0,
			status: MALStatuses["Plan To Watch"],
			score: 0
		});
	}
	static async removeAnime(anime: AnimeEntry & HasMalId): Promise<boolean> {
		const account = await this.getMalAccount();
		const ok = await account.anime.deleteMyAnime(anime.malId).call().catch(() => false);
		anime.syncGet();
		anime.myMalStatus = undefined;
		anime.myWatchedEpisodes = undefined;
		anime.myMalRating = undefined;
		anime.myRewatchAmount = undefined;
		anime.syncPut(true);
		Consts.MAL_USER.animeList.loadAnime(anime);
		Consts.setMALUser(Consts.MAL_USER);
		if (ok === false)
			return ok;
		return true;
	}
	static async UpdateWatchedEpisode(downloadedItem: DownloadedItem): Promise<boolean> {
		downloadedItem.animeEntry.syncGet();
		if (!downloadedItem.animeEntry.malId) return false;
		let episode = downloadedItem.episodeData.episodeOrMovieNumber,
			status = downloadedItem.animeEntry.totalEpisodes === episode ? MALStatuses.Completed : MALStatuses.Watching;
		const ok = await MALUtils.updateAnime(downloadedItem.animeEntry as AnimeEntry & HasMalId, { episodes: episode, status });
		Consts.MAL_USER.animeList.loadAnime(downloadedItem.animeEntry);
		Consts.setMALUser(Consts.MAL_USER);
		return ok;
	}
	static async animeStats(anime: AnimeEntry & HasMalId) {
		const response = await fetch(`https://myanimelist.net/anime/${anime.malId}/${anime.name}/stats`).then(r => r.text()),
			html = new DOMParser().parseFromString(response, "text/html");
		const summary: Record<string, string> = [...html.querySelectorAll(".js-scrollfix-bottom-rel > .spaceit_pad")].reduce(
			(a, b) => ({ [b.childNodes[0].textContent?.slice(0, - 1) ?? ""]: b.childNodes[1].textContent, ...a }), {});
		return {
			Watching: summary.Watching,
			Completed: summary.Completed,
			"On-Hold": summary["On-Hold"],
			Dropped: summary.Dropped,
			"Plan to Watch": summary["Plan to Watch"],
			Total: summary.Total,
			scores: [...html.querySelectorAll('.js-scrollfix-bottom-rel table tr')].reduce(
				(acc, b) => ({ [b.children?.[0]?.textContent ?? ""]: b.querySelector("span")?.childNodes?.[0]?.nodeValue ?? "", ...acc }), {} as Record<string, string>)
		};

	}
	static async animeForum(anime: AnimeEntry & HasMalId, page: number = 0): Promise<[ForumTopic[], number]> {
		const response = await fetch(`https://myanimelist.net/forum/?animeid=${anime.malId}&show=${page * 50}`).then(r => r.text()),
			html = document.createElement("html");
		html.innerHTML = response;
		if (response.trim().length === 0)
			return MALUtils.animeForum(anime, page);
		const rows = [...html.querySelectorAll("tbody tr:not(.forum-table-header)")];
		const data = rows.map(row => {
			const author = row.querySelector("a[href*='/profile/']"),
				title: HTMLAnchorElement | null = row.querySelector("a[href*='/forum/']:not([title=\"Go to Newest Post\"])"),
				posted = row.querySelector("td:last-child"),
				replies = posted ? posted.previousElementSibling : posted;
			return {
				author: author ? author.textContent || "" : "",
				title: title ? title.textContent || "" : "",
				url: title ? title.href.replace(window.location.origin, "https://myanimelist.net") : "",
				posted: posted && posted.lastChild ? posted.lastChild.textContent || "" : "",
				replies: replies ? Number(replies.textContent) || 0 : 0
			};
		}),
			numOfPages = html.querySelector("#content .borderClass .di-ib");
		data.sort((a, b) => {
			const aDiscussion = a.title.includes("Discussion"),
				bDiscussion = b.title.includes("Discussion");
			return aDiscussion && bDiscussion ? b.title.localeCompare(a.title, "en-us", { numeric: true }) : bDiscussion ? 1 : -1;
		});
		return [data, numOfPages && numOfPages.firstChild ? Number((numOfPages.firstChild.textContent || "").match(/(?<=Pages \()\d(?=\))/g)) - 1 : 0];
	}
	static async forumEntry(topic: ForumTopic): Promise<ForumEntry> {
		let response = await fetch(topic.url).then(r => r.text());
		let html = document.createElement("html");
		html.innerHTML = response;
		let messages = [...html.querySelectorAll(".forum_border_around[id*=\"forumMsg\"]")].map(msg => {
			let userData = msg.querySelector("td:first-child")!.textContent!.split("\n").map(ele => ele.trim()).filter(ele => ele.length),
				msgHTML = msg.querySelector("[id^=\"message\"]:not([id^=\"messageuser\"]).clearfix")!.innerHTML,
				msgId = Number(msg.querySelector("[id^=\"message\"]:not([id^=\"messageuser\"]).clearfix")!.id.replace("message", ""));
			msgHTML = msgHTML.replace(/<iframe/g, "<iframe allowfullscreen").replace(/href="\/forum\/message\/[0-9]+\?goto=topic/g, m => "href=\"#" + m.replace("href=\"/forum/message/", "").replace("?goto=topic", ""));
			return new ForumMessage({
				messageHTML: msgHTML,
				time: parseStupidAmericanDateString(msg.querySelector(".date")!.textContent as string),
				user: {
					imageURL: (msg.querySelector(".forum-icon img") || {} as any).src,
					posts: Number(userData[3].replace("Posts: ", "")),
					joined: userData[2].replace("Joined: ", ""),
					status: userData[1] as any,
					name: userData[0]
				},
				id: msgId
			});
		});
		return new ForumEntry({
			messages: messages, title: topic.title
		});
	}
	static async forumOfEpisode(episode: DownloadedItem): Promise<ForumEntry | undefined> {
		if (!episode.animeEntry?.malId) return;
		async function findForumTopic(pageNum: number = 0): Promise<ForumTopic | undefined> {
			const [topics, numOfPages] = await MALUtils.animeForum(episode.animeEntry as any, pageNum),
				relaventTopic = topics.find(topic => topic.title.endsWith(`Episode ${episode.episodeData.episodeOrMovieNumber} Discussion`));
			if (!relaventTopic) {
				const episodeNums = topics.map(topic => Number(topic.title.match(/Episode ([0-9]+) Discussion$/)?.[0])).filter(ele => !isNaN(ele)),
					highestEpisodeNum = Math.max(...episodeNums);
				if (episode.episodeData.episodeOrMovieNumber > highestEpisodeNum && numOfPages > pageNum)
					return findForumTopic(pageNum + 1);
			}
			return relaventTopic;
		}
		const forumTopic = await findForumTopic();
		if (forumTopic)
			return await MALUtils.forumEntry(forumTopic);
	}
	static async animeReviews(anime: AnimeEntry & HasMalId): Promise<Reviews | undefined> {
		const data = await mal.findAnime(anime.malId, "reviews");
		data.reviews.forEach((review: any) => {
			review.date = new Date(review.date);
		});
		return data;
	}
	static async animeRecommandation(anime: AnimeEntry & HasMalId): Promise<AnimeRecommandation[] | void> {
		const data = await fetch(`https://myanimelist.net/anime/${anime.malId}/asd/userrecs`).then(ele => ele.text()),
			html = document.createElement("html");
		html.innerHTML = data;
		const recommandations: AnimeRecommandation[] = [...html.querySelectorAll(".js-scrollfix-bottom-rel > .borderClass")].map(ele => {
			const recommandedAnimeId = Number(ele.querySelector("a")?.href.match(/(?<=\/)[0-9]+(?=\/)/g));
			return {
				animeRecommanded: new AnimeEntry({
					malId: recommandedAnimeId,
					imageURL: (ele.querySelector("a > img[srcset]") as any)?.src,
					name: ele.querySelector("td:nth-child(2) > div > a")?.textContent ?? ""
				}),
				recommandationEntries: [...ele.querySelectorAll(".borderClass")].map(ele => {
					return {
						recommandedUsername: ele.children[1].querySelector("a[href*='/profile/']")?.innerHTML ?? "",
						recommandedText: ele.children[0].textContent ?? ""
					};
				})
			};
		});
		return recommandations;
	}
}

interface RecommandationEntry {
	recommandedUsername: string;
	recommandedText: string;
}

interface AnimeRecommandation {
	animeRecommanded: AnimeEntry;
	recommandationEntries: RecommandationEntry[];
}

export interface ForumTopic {
	posted: string;
	author: string;
	url: string;
	title: string;
	replies: number;
}

export class ForumEntry {
	title: string;
	messages: ForumMessage[];
	constructor({
		title,
		messages
	}: {
		title: string,
		messages: ForumMessage[];
	}) {
		this.title = title;
		this.messages = messages;
	}
}

class ForumMessage {
	messageHTML: string;
	user: { name: string; imageURL?: string; status: "offline" | "online"; joined: string; posts: number; };
	time: Date;
	id: number;
	constructor({
		messageHTML,
		user,
		time,
		id
	}:
		{
			messageHTML: string,
			user: {
				name: string,
				imageURL?: string,
				status: "offline" | "online",
				joined: string,
				posts: number;
			},
			time: Date,
			id: number;
		}) {
		this.messageHTML = messageHTML;
		this.user = user;
		this.time = time;
		this.id = id;
	}
}
(window as any).MALUtils = MALUtils;
