const electron = require('electron'),
	url = require("url"),
	path = require("path"),
	isDev = require("electron-is-dev"),
	process = require("process"),
	fsPromises = require("fs").promises,
	{ extractSubsFromVideoFile } = require("./mkvExtract"),
	extractionsOfSubs = [],
	{ walkDir } = require("./walkDir");

process.on('uncaughtException', () => { });

electron.ipcMain.handle("walkDir", async (_, path) => {
	return await walkDir(path);
});

electron.ipcMain.on("stop-processing-of-sub-file", () => {
	extractionsOfSonubs.forEach(ele => ele.destroy());
	extractionsOfSubs.splice(0, extractionsOfSubs.length);
});
electron.ipcMain.on("process-subs-of-file", async (event, filePath) => {
	if (extractionsOfSubs.length) {
		extractionsOfSubs.forEach(ele => ele.destroy());
		extractionsOfSubs.splice(0, extractionsOfSubs.length);
	}
	extractionsOfSubs.push(await extractSubsFromVideoFile(filePath, files => event.reply("sub-file-processed", files)));
});

const subsFileExtensions = new Set([".ass", ".srt"]);

electron.ipcMain.handle("get-subs-of-file", async (_, filePath) => {
	const { dir, name: originalFileName } = path.parse(filePath),
		dirItems = await fsPromises.readdir(dir),
		parsedFiltered = dirItems.map(ele => path.parse(ele)).filter(parsed => {
			return parsed.name === originalFileName && subsFileExtensions.has(parsed.ext);
		});
	return Promise.all(parsedFiltered.map(async parsed => ({
		name: parsed.name,
		title: parsed.name,
		data: await fsPromises.readFile(path.join(dir, parsed.base), "utf-8")
	})));
});

// Module to control application life.
const app = electron.app;

app.commandLine.appendSwitch('disable-site-isolation-trials');
app.commandLine.appendSwitch('disable-features', 'OutOfBlinkCors');

app.whenReady().then(() => {
	electron.protocol.registerFileProtocol('file', (request, callback) => {
		const pathname = decodeURI(request.url.replace('file:///', ''));
		callback(pathname);
	});
});

// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

const windowStateKeeper = require('electron-window-state');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow, webTorrentWindow;

function createWindow() {
	// Create the browser window.

	let mainWindowState = windowStateKeeper({
		defaultWidth: 800,
		defaultHeight: 600,
		fullScreen: false
	});

	mainWindow = new BrowserWindow({
		x: mainWindowState.x,
		y: mainWindowState.y,
		width: mainWindowState.width,
		height: mainWindowState.height,
		backgroundColor: "#3e3e3e",
		webPreferences: {
			webSecurity: false,
			enableRemoteModule: true,
			allowRunningInsecureContent: false,
			nodeIntegration: true,
			devTools: isDev,
			experimentalFeatures: true,
			plugins: true
		},
		show: true,
		autoHideMenuBar: true
	});
	mainWindow.webContents.session.webRequest.onHeadersReceived((details, callback) => {
		callback({ responseHeaders: Object.fromEntries(Object.entries(details.responseHeaders).filter(header => !/x-frame-options/i.test(header[0]))) });
	});

	mainWindowState.manage(mainWindow);

	webTorrentWindow = new BrowserWindow({
		show: false,
		webPreferences: {
			nodeIntegration: true,
			webSecurity: false,
		}
	});

	webTorrentWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'webtorrent', 'webtorrent.html'),
		protocol: 'file'
	}));

	require("./webtorrent/webtorrentMain").init(mainWindow, webTorrentWindow);

	if (isDev)
		mainWindow.loadURL(url.format({
			pathname: 'localhost:3000',
			protocol: 'http'
		}));
	else {
		mainWindow.loadURL(url.format({
			pathname: path.join(__dirname, 'index.html'),
			protocol: 'file'
		}));
	}

	// Emitted when the window is closed.
	mainWindow.on('closed', function () {
		// Dereference the window object, usually you would store windows
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		mainWindow = null;
		if (webTorrentWindow)
			webTorrentWindow.close();
	});

	webTorrentWindow.on('closed', function () {
		webTorrentWindow = null;
		if (mainWindow)
			mainWindow.close();
	});

	mainWindow.webContents.on('new-window', function (e, url) {
		// make sure local urls stay in electron perimeter
		if ('file://' === url.substr(0, 'file://'.length)) {
			return;
		}

		// and open every other protocols on the browser      
		e.preventDefault();
		electron.shell.openExternal(url);
	});
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
	// On OS X it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
