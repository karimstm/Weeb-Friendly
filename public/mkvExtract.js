// Adapted from https://github.com/qgustavor/mkv-extract/ licensed under MIT
const fs = require("fs"),
	Matroska = require("matroska-subtitles"),
	{ fetch } = require("fetch-h2");

async function streamFromURL(url) {
	const response = await fetch(url);
	return response.readable();
}

async function extractSubsFromVideoFile(file, atData) {
	if (file.startsWith('file://')) {
		file = file.substring(7);
		const stat = await fs.promises.stat(file),
			{ tracks, parser } = await getTracksOfFile(file, fs.createReadStream),
			fileStream = fs.createReadStream(file, { highWaterMark: (stat.blksize * stat.blocks) / 256 });
		createDecoderFromStream(fileStream, atData, tracks, parser);
		return {
			destroy: () => fileStream.destroy()
		};
	}
	const { tracks, parser } = await getTracksOfFile(file, streamFromURL),
		fileStream = await streamFromURL(file);
	createDecoderFromStream(fileStream, atData, tracks, parser);
	return {
		destroy: () => fileStream.destroy()
	};
}

function getTracksOfFile(file, createStreamMethod) {
	return new Promise(async resolve => {
		const stream = await createStreamMethod(file);
		const parser = new Matroska();
		stream.pipe(parser);
		parser.once('tracks', rawTracks => {
			const tracks = {};
			for (const track of rawTracks)
				tracks[track.number] = track;
			resolve({ tracks, parser });
		});
	});
}

function createDecoderFromStream(stream, atData, tracks, prevDecoder) {
	const matroskaDecoder = new Matroska(prevDecoder);
	let emitTimeout,
		emitTimeoutStop = false,
		emitTimeoutMiliseconds = 5000,
		thereIsAnEmitWaiting = false;
	matroskaDecoder.on("subtitle", (subtitle, trackNumber) => {
		let track = tracks[trackNumber];
		if (!track.subtitles)
			track.subtitles = [];
		track.subtitles.push(subtitle);
		thereIsAnEmitWaiting = true;
		if (emitTimeoutStop) return;
		thereIsAnEmitWaiting = false;
		emitTimeoutStop = true;
		clearTimeout(emitTimeout);
		emitTimeout = setTimeout(() => {
			emitTimeoutStop = false;
			if (thereIsAnEmitWaiting)
				emitData();
		}, emitTimeoutMiliseconds);
		emitData();
	});
	stream.pipe(matroskaDecoder);
	return {
		decoder: matroskaDecoder,
		promise: new Promise(resolve => {
			matroskaDecoder.once("finish", () => { emitData(); resolve(tracks); });
		})
	};
	function emitData() {
		let files = [];
		Object.values(tracks).forEach(track => {
			const heading = track.header;
			const isASS = heading.includes("Format:");
			const formatFn = isASS ? formatTimestamp : formatTimestampSRT;
			const eventMatches = isASS
				? heading.match(/\[Events\]\s+Format:([^\r\n]*)/)
				: [""];
			const headingParts = isASS ? heading.split(eventMatches[0]) : ["", ""];
			const fixedLines = [];
			if (track.subtitles)
				track.subtitles.forEach((subtitle, i) => {
					let startTimeStamp = formatFn(subtitle.time),
						endTimeStamp = formatFn(subtitle.time + subtitle.duration),
						lineParts = [subtitle.layer, startTimeStamp, endTimeStamp, subtitle.style,
						subtitle.name, subtitle.marginL, subtitle.marginR, subtitle.marginV, subtitle.effect, subtitle.text],
						fixedLine = isASS ? "Dialogue: " + lineParts.join(",")
							: i + 1 + "\r\n" + startTimeStamp.replace(".", ",") +
							" --> " + endTimeStamp.replace(".", ",") + "\r\n" + subtitle.text + "\r\n";
					if (fixedLines[i]) {
						fixedLines[i] += "\r\n" + fixedLine;
					} else {
						fixedLines[i] = fixedLine;
					}
				});
			let data = (isASS ? headingParts[0] + eventMatches[0] + "\r\n" : "") + fixedLines.join("\r\n") + headingParts[1] + "\r\n",
				extName = isASS ? ".ass" : ".srt";
			files.push({
				name: track.language ? track.language + extName : "Subtitle " + track.number + extName,
				data,
				title: track.header.split("\n")[1].substring(7),
				tracks,
				subtitles: track.subtitles
			});
		});
		atData(files);
	}
}

function formatTimestamp(timestamp) {
	const seconds = timestamp / 1000;
	const hh = Math.floor(seconds / 3600);
	let mm = Math.floor((seconds - hh * 3600) / 60);
	let ss = (seconds - hh * 3600 - mm * 60).toFixed(2);

	if (mm < 10) mm = `0${mm}`;
	if (ss < 10) ss = `0${ss}`;

	return `${hh}:${mm}:${ss}`;
}

function formatTimestampSRT(timestamp) {
	const seconds = timestamp / 1000;
	let hh = Math.floor(seconds / 3600);
	let mm = Math.floor((seconds - hh * 3600) / 60);
	let ss = (seconds - hh * 3600 - mm * 60).toFixed(3);

	if (hh < 10) hh = `0${hh}`;
	if (mm < 10) mm = `0${mm}`;
	if (ss < 10) ss = `0${ss}`;

	return `${hh}:${mm}:${ss}`;
}

module.exports = { extractSubsFromVideoFile };
